/*
 * Copyright (C) 2018 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.domain.location.interactor

import com.xme.client.android.ultravioletindex.domain.interactor.MaybeUseCase
import com.xme.client.android.ultravioletindex.domain.location.repository.LocationRepository
import com.xme.client.android.ultravioletindex.model.location.Location
import io.reactivex.rxjava3.core.Maybe

/**
 * Use case for retrieving the mobile device last known location from the [LocationRepository].
 */
class GetLastLocationUseCase(private val locationRepository: LocationRepository) :
    MaybeUseCase<Location, Void>() {

    override fun buildUseCaseObservable(params: Void?): Maybe<Location> {
        return locationRepository.getLastLocation()
    }
}