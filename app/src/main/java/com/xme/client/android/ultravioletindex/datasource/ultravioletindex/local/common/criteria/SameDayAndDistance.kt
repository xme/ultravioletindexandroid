/*
 * Copyright (C) 2018 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.datasource.ultravioletindex.local.common.criteria

import com.xme.client.android.ultravioletindex.data.location.util.GeoUtils
import com.xme.client.android.ultravioletindex.data.util.DateUtils
import com.xme.client.android.ultravioletindex.model.ultravioletindex.CurrentUltravioletIndex
import io.reactivex.rxjava3.core.Single
import timber.log.Timber
import java.time.Instant

class SameDayAndDistance : CacheCriterion<CurrentUltravioletIndex> {

    companion object {
        private const val CACHE_DISTANCE_THRESHOLD = 50000.0 // 50 Km in meters
    }

    override fun isValidCache(
        data: CurrentUltravioletIndex,
        latitude: Double,
        longitude: Double
    ): Single<Boolean> {
        return Single.defer {
            val cacheInstant = Instant.ofEpochSecond(data.date)
            val sameDay =
                DateUtils.isSameDay(cacheInstant, Instant.now(), data.timeZoneId)
            val distance = GeoUtils.computeDistance(
                data.latitude,
                data.longitude,
                latitude,
                longitude
            )

            Timber.d("Cache validation: time is \"$sameDay\" and distance is \"${distance <= CACHE_DISTANCE_THRESHOLD}\"")

            Single.just(sameDay && (distance <= CACHE_DISTANCE_THRESHOLD))
        }
    }
}