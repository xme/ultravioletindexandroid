/*
 * Copyright (C) 2018 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.datasource.ultravioletindex.local.sharedpreferences

import android.content.Context
import android.content.SharedPreferences
import com.xme.client.android.ultravioletindex.injection.Injection

/**
 * Shared preferences helper class used for storing values.
 */
class SharedPreferencesHelper {

    companion object {
        private const val FILE_NAME = "com.xme.client.android.ultravioletindex.datasource.ultravioletindex.cache"
        private const val CURRENT_ULTRAVIOLET_INDEX_KEY = "currentUltravioletIndex"
    }

    private val sharedPreferences: SharedPreferences

    init {
        val context = Injection.provideApplicationContext()
        sharedPreferences = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE)
    }

    /**
     * Store and retrieve the last time data was cached
     */
    var currentUltravioletIndex: String
        get() = sharedPreferences.getString(CURRENT_ULTRAVIOLET_INDEX_KEY, "") ?: ""
        set(json) = sharedPreferences.edit().putString(CURRENT_ULTRAVIOLET_INDEX_KEY, json).apply()
}
