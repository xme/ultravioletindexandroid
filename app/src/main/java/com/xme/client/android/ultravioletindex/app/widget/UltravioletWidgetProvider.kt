/*
 * Copyright (C) 2018 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.app.widget

import android.app.AlarmManager
import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.BroadcastReceiver
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.SystemClock
import androidx.core.app.JobIntentService
import com.xme.client.android.ultravioletindex.BuildConfig
import timber.log.Timber

/**
 * Entry point for a simple widget that displays current ultraviolet index.
 *
 * This class responsibility is to handle update operations for one or multiple widgets. Updates
 * can be requested by Android through [onUpdate], or can be forced for development purposes with
 * [scheduleNextUpdate] using an alarm manager. Currently, [onUpdate] is configured to be called
 * every hour with updatePeriodMillis property in widget XML descriptor.
 *
 * [UltravioletWidgetUpdateService] is a [JobIntentService] spawned to perform the asynchronous
 * update of the widget in background.
 *
 * IMPORTANT: The alarm manager solution, for multiple widgets, needs a way of obtaining the
 * updated list of widget ids. For example, if you have two widgets and you remove one, the
 * onDelete() function will be called with only the list of deleted widgets, but the pending
 * intent cannot be retrieved for removing the deleted widget id from its list of ids. In order
 * to obtain an updated list of widget ids at any moment, [getWidgetIds] is used. Enabling two
 * options:
 * a) the [onUpdate] function can be used in production, and the alarm manager can be only used in
 * development for debugging,
 * b) the alarm manager can be used in production with the updated list
 * of ids, too.
 * The advantages of the later solution are:
 * 1) the possibility of updating the widget only when the phone is awake without forcing the
 * phone to wake for updating (as it happens with [onUpdate]),
 * 2) the ability to update the widget with a higher frequency than the minimum 30 minutes imposed
 * by Android, which in this case it is not needed.
 */
class UltravioletWidgetProvider : AppWidgetProvider() {

    override fun onReceive(context: Context, intent: Intent) {
        // Chain up to the super class so the onEnabled() an other callbacks get dispatched
        super.onReceive(context, intent)
        // Handle a different Intent
        Timber.d("%s", intent.action)

        intent.action?.let {
            Timber.i("onReceive ${intent.action}")

            // Implement custom actions
            when (it) {
                ACTION_WIDGET_REFRESH -> {
                    val updateIntent = Intent(context, UltravioletWidgetUpdateService::class.java)
                    updateIntent.putExtra(UltravioletWidgetUpdateService.WIDGET_IDS_KEY, getWidgetIds(context))
                    UltravioletWidgetUpdateService.enqueueWork(context, updateIntent)
                }
                else -> {
                    //Timber.e("Widget provider action ${intent.action} not implemented")
                }
            }
        }
    }

    // This function will not be called more often than every 30 minutes, no matter what is set in
    // updatePeriodMillis inside the widget XML descriptor.
    // See: https://stackoverflow.com/questions/2078122/android-widget-not-updating
    // If more frequent updates are needed, and/or the device should not wake up when it is asleep,
    // use an alarm as explained here:
    // https://stackoverflow.com/questions/5476867/updating-app-widget-using-alarmmanager/14319020#14319020
    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        Timber.d("Updating...")

        // To prevent any ANR timeouts, updates are performed in a service. However, there are
        // severe restrictions to execute things in background starting from Android 8.0. The
        // solution is to enqueue work in a JobIntent. This new kind of retro-compatible service
        // will handle everything for us.
        // See: https://android.jlelse.eu/keep-those-background-services-working-when-targeting-android-oreo-sdk-26-cbf6cc2bdb7f

        // Update now the widget
        val intent = Intent(context, UltravioletWidgetUpdateService::class.java)
        intent.putExtra(UltravioletWidgetUpdateService.WIDGET_IDS_KEY, appWidgetIds)
        enqueueWork(context, intent)

        if (BuildConfig.DEBUG) {
            // For developing and debugging purposes, an alarm manager will be used to send a broadcast,
            // whose receiver will enqueue work in the JobIntentService. With the alarm manager the
            // smallest time interval is 1 minute, with the app widget provider the smallest time
            // interval is 30 minutes.
            scheduleNextUpdate(context)
        }
    }

    override fun onDeleted(context: Context, appWidgetIds: IntArray) {
        Timber.d("Deleting...")

        if (BuildConfig.DEBUG) {
            scheduleNextUpdate(context)
        }
    }

    private fun scheduleNextUpdate(context: Context) {
        // Schedule next update with an alarm manager instead of waiting for the next call of this
        // onUpdate() function.
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE)
        if (alarmManager is AlarmManager) {
            val alarmManagerIntent = Intent(context, UltravioletWidgetReceiver::class.java)
            val appWidgetIds = getWidgetIds(context)
            alarmManagerIntent.putExtra(UltravioletWidgetUpdateService.WIDGET_IDS_KEY, appWidgetIds)
            // Build the PendingIntent used to trigger the action
            val pendingIntent = PendingIntent.getBroadcast(context, 0, alarmManagerIntent, PendingIntent.FLAG_UPDATE_CURRENT)
            if (appWidgetIds.isNotEmpty()) {
                alarmManager.setRepeating(
                    AlarmManager.ELAPSED_REALTIME,
                    SystemClock.elapsedRealtime(),
                    UPDATE_INTERVAL,
                    pendingIntent
                )
            } else {
                // If there are no widgets, cancel the pending intent
                alarmManager.cancel(pendingIntent)
            }
        } else {
            Timber.e("Alarm manager cannot be obtained")
        }
    }

    private fun getWidgetIds(context: Context): IntArray {
        // See:
        // https://stackoverflow.com/a/5649667/5189200
        // https://stackoverflow.com/a/17387978/5189200
        val name = ComponentName(context, UltravioletWidgetProvider::class.java)
        return AppWidgetManager.getInstance(context).getAppWidgetIds(name)
    }

    /////////////////////////////////////////////
    // Broadcast receiver
    /////////////////////////////////////////////

    // IMPORTANT: Remember to register the broadcast receiver in the Manifest.
    class UltravioletWidgetReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent?) {
            Timber.d("Broadcast received")

            if (intent != null) {
                UltravioletWidgetProvider.enqueueWork(context, intent)
            } else {
                Timber.e("Intent is null. Nothing will be done.")
            }
        }
    }

    companion object {
        private const val UPDATE_INTERVAL: Long = 60000 // in milliseconds
        const val ACTION_WIDGET_REFRESH = "ActionWidgetRefresh"

        /**
         * Convenience method for enqueuing work in [UltravioletWidgetUpdateService].
         */
        private fun enqueueWork(context: Context, intent: Intent) {
            // Just change intent class
            intent.setClass(context, UltravioletWidgetUpdateService::class.java)
            // And enqueue in JobIntentService
            UltravioletWidgetUpdateService.enqueueWork(context, intent)
        }
    }
}
