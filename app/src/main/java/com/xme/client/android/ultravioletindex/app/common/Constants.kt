/*
 * Copyright (C) 2018 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.app.common

const val LOCATION_PERMISSION = android.Manifest.permission.ACCESS_FINE_LOCATION
const val PRIVACY_URL = "https://gl.githack.com/xme/ultravioletindexandroid/raw/master/extras/legal/privacyPolicy.html"
const val TERMS_URL = "https://gl.githack.com/xme/ultravioletindexandroid/raw/master/extras/legal/termsAndConditions.html"