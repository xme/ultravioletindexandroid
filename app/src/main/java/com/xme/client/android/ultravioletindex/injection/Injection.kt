/*
 * Copyright (C) 2018 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.injection

import android.content.Context
import com.xme.client.android.ultravioletindex.AndroidApplication
import com.xme.client.android.ultravioletindex.data.location.LocationDataRepository
import com.xme.client.android.ultravioletindex.data.ultravioletindex.UltravioletIndexDataRepository
import com.xme.client.android.ultravioletindex.data.ultravioletindex.source.UltravioletIndexDataStoreFactory
import com.xme.client.android.ultravioletindex.datasource.location.AndroidLocationDataStore
import com.xme.client.android.ultravioletindex.datasource.location.mapper.AndroidLocationMapper
import com.xme.client.android.ultravioletindex.datasource.ultravioletindex.local.common.criteria.SameDayAndDistance
import com.xme.client.android.ultravioletindex.datasource.ultravioletindex.local.sharedpreferences.SharedPreferencesHelper
import com.xme.client.android.ultravioletindex.datasource.ultravioletindex.local.sharedpreferences.SharedPreferencesUltravioletIndexDataStore
import com.xme.client.android.ultravioletindex.datasource.ultravioletindex.local.sharedpreferences.mapper.SharedPreferencesCurrentUltravioletIndexMapper
import com.xme.client.android.ultravioletindex.datasource.ultravioletindex.remote.openweather.OpenWeatherDataStore
import com.xme.client.android.ultravioletindex.domain.location.repository.LocationRepository
import com.xme.client.android.ultravioletindex.domain.ultravioletindex.repository.UltravioletIndexRepository

/**
 * Simple injection mechanism.
 */
object Injection {

    private val androidLocationMapper by lazy {
        AndroidLocationMapper()
    }

    private val androidLocationDataStore by lazy {
        AndroidLocationDataStore(androidLocationMapper)
    }

    private val locationDataRepository by lazy {
        LocationDataRepository(
            androidLocationDataStore
        )
    }

    private val openWeatherDataStore by lazy {
        OpenWeatherDataStore()
    }

    private val sharedPreferencesHelper by lazy {
        SharedPreferencesHelper()
    }

    private val sharedPreferencesCurrentUltravioletIndexMapper by lazy {
        SharedPreferencesCurrentUltravioletIndexMapper()
    }

    private val cacheCriterion by lazy {
        SameDayAndDistance()
    }

    private val sharedPreferencesUltravioletIndexDataStore by lazy {
        SharedPreferencesUltravioletIndexDataStore(
            sharedPreferencesHelper,
            sharedPreferencesCurrentUltravioletIndexMapper,
            cacheCriterion
        )
    }

    private val ultravioletIndexDataStoreFactory by lazy {
        UltravioletIndexDataStoreFactory(sharedPreferencesUltravioletIndexDataStore, openWeatherDataStore)
    }

    private val ultravioletIndexDataRepository by lazy {
        UltravioletIndexDataRepository(
            ultravioletIndexDataStoreFactory
        )
    }

    fun provideApplicationContext(): Context {
        return AndroidApplication.applicationContext()
    }

    fun provideLocationRepository(): LocationRepository {
        return locationDataRepository
    }

    fun provideUltravioletIndexRepository(): UltravioletIndexRepository {
        return ultravioletIndexDataRepository
    }
}