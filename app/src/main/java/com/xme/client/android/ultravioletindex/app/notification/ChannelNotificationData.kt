/*
 * Copyright (C) 2018 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.app.notification

/**
 * Represents channel data needed for Android 26 and above.
 */
interface ChannelNotificationData {

    // System channel name
    val channelId: String
    // The user-visible name of the channel
    val channelName: CharSequence
    // The user-visible description of the channel
    val channelDescription: String
    val channelImportance: Int
    val isChannelEnableVibrate: Boolean
    val isChannelEnableLight: Boolean
    val channelLightColor: Int
    val channelLockScreenVisibility: Int
}
