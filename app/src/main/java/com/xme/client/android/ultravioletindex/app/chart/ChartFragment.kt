/*
 * Copyright (C) 2019 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.app.chart

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.github.mikephil.charting.charts.CombinedChart.DrawOrder
import com.github.mikephil.charting.components.XAxis.XAxisPosition
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.CombinedData
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import com.google.android.material.color.MaterialColors
import com.xme.client.android.ultravioletindex.R
import com.xme.client.android.ultravioletindex.app.chart.mpandroidchart.FloatValueFormatter
import com.xme.client.android.ultravioletindex.app.chart.mpandroidchart.IntValueFormatter
import com.xme.client.android.ultravioletindex.app.common.error.ErrorBundleBuilder
import com.xme.client.android.ultravioletindex.app.common.error.RetryAction
import com.xme.client.android.ultravioletindex.app.common.error.RetryAction.GetLastLocation
import com.xme.client.android.ultravioletindex.app.common.error.RetryAction.GetUltravioletIndex
import com.xme.client.android.ultravioletindex.app.common.error.RetryAction.RefreshWidget
import com.xme.client.android.ultravioletindex.app.common.event.Event
import com.xme.client.android.ultravioletindex.app.common.exception.LocationServiceNoPermissionException
import com.xme.client.android.ultravioletindex.app.common.internationalization.NumberFormatHelper
import com.xme.client.android.ultravioletindex.app.common.state.LocationServiceState
import com.xme.client.android.ultravioletindex.app.common.state.LocationServiceState.Available
import com.xme.client.android.ultravioletindex.app.common.state.LocationServiceState.Failure.BackgroundLocationNoPermission
import com.xme.client.android.ultravioletindex.app.common.state.LocationServiceState.Failure.Error
import com.xme.client.android.ultravioletindex.app.common.state.LocationServiceState.Failure.LocationServiceNoPermission
import com.xme.client.android.ultravioletindex.app.common.state.LocationServiceState.Failure.LocationServiceOff
import com.xme.client.android.ultravioletindex.app.common.state.LocationServiceState.Loading
import com.xme.client.android.ultravioletindex.app.common.state.UltravioletIndexState
import com.xme.client.android.ultravioletindex.app.common.util.MathUtils
import com.xme.client.android.ultravioletindex.app.common.util.ResourceUtils
import com.xme.client.android.ultravioletindex.app.common.view.error.ErrorListener
import com.xme.client.android.ultravioletindex.app.common.view.index.IndexBaseFragment
import com.xme.client.android.ultravioletindex.app.index.IndexViewModel
import com.xme.client.android.ultravioletindex.app.index.dialog.DialogAction.OPEN_GOOGLE_MAPS
import com.xme.client.android.ultravioletindex.app.index.dialog.DialogAction.OPEN_LOCATION_SETTINGS
import com.xme.client.android.ultravioletindex.app.index.error.IndexErrorBundleBuilder
import com.xme.client.android.ultravioletindex.databinding.ChartFragmentBinding
import com.xme.client.android.ultravioletindex.model.location.exception.NoLocationAvailableException
import com.xme.client.android.ultravioletindex.model.ultravioletindex.CurrentUltravioletIndex
import com.xme.client.android.ultravioletindex.model.ultravioletindex.exception.UviNotComputableException
import timber.log.Timber
import java.time.format.DateTimeFormatter
import java.util.ArrayList

class ChartFragment : IndexBaseFragment() {

    private var _binding: ChartFragmentBinding? = null
    private val binding get() = _binding!!

    private val viewModel: IndexViewModel by activityViewModels()
    private lateinit var errorBundleBuilder: ErrorBundleBuilder

    //region Life cycle
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = ChartFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun initializeViews(savedInstanceState: Bundle?) {
        super.initializeViews(savedInstanceState)

        initializeErrorView()

        val textColor = MaterialColors.getColor(binding.flChartContainer, R.attr.colorOnSecondary)

        // Configure chart
        binding.ccChartCombinedChart.description.isEnabled = false
        //val backgroundColor = MaterialColors.getColor(binding.flChartContainer, R.attr.colorOnPrimary)
        //binding.ccChartCombinedChart.setBackgroundColor(backgroundColor)
        binding.ccChartCombinedChart.setDrawGridBackground(false)
        binding.ccChartCombinedChart.setDrawBarShadow(false)
        binding.ccChartCombinedChart.isHighlightFullBarEnabled = false

        // draw bars behind lines
        binding.ccChartCombinedChart.drawOrder = arrayOf(DrawOrder.BAR, DrawOrder.LINE)

        // Configure legend
        binding.ccChartCombinedChart.legend.isEnabled = false
//        binding.ccChartCombinedChart.legend.isWordWrapEnabled = true
//        binding.ccChartCombinedChart.legend.verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
//        binding.ccChartCombinedChart.legend.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
//        binding.ccChartCombinedChart.legend.orientation = Legend.LegendOrientation.HORIZONTAL
//        binding.ccChartCombinedChart.legend.setDrawInside(false)

        // Configure Y axis
        binding.ccChartCombinedChart.axisRight.setDrawGridLines(false)
        binding.ccChartCombinedChart.axisRight.axisMinimum = 0f // this replaces setStartAtZero(true)
        binding.ccChartCombinedChart.axisRight.textColor = textColor
        binding.ccChartCombinedChart.axisLeft.setDrawGridLines(false)
        binding.ccChartCombinedChart.axisLeft.axisMinimum = 0f // this replaces setStartAtZero(true)
        binding.ccChartCombinedChart.axisLeft.textColor = textColor

        // Configure X Axis
        binding.ccChartCombinedChart.xAxis.position = XAxisPosition.BOTH_SIDED
        binding.ccChartCombinedChart.xAxis.spaceMin = 0.5f // Add spacing for first bar, so that it is not cropped
        binding.ccChartCombinedChart.xAxis.spaceMax = 0.5f // Add spacing for last bar, so that it is not cropped
        //binding.ccChartCombinedChart.xAxis.axisMinimum = 0f
        //binding.ccChartCombinedChart.xAxis.axisMaximum = 23f
        binding.ccChartCombinedChart.xAxis.granularity = 1f
        binding.ccChartCombinedChart.xAxis.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                return "${value.toInt().toString().padStart(2, '0')}:00"
            }
        }
        binding.ccChartCombinedChart.xAxis.textColor = textColor
    }

    override fun initializeContents(savedInstanceState: Bundle?) {
        super.initializeContents(savedInstanceState)

        // Listen to view model events published by the dialogs shown in this fragment
        listenToGenericDialog()

        // Link the fragment and the view model with "viewLifecycleOwner", so that observers
        // can be subscribed in onActivityCreated() and can be automatically unsubscribed
        // in onDestroyView().
        // IMPORTANT: Never use "this" as lifecycle owner.
        // See: https://medium.com/@BladeCoder/architecture-components-pitfalls-part-1-9300dd969808
        viewModel.isLocationProviderAvailableEvent.observe(viewLifecycleOwner, Observer<Event<LocationServiceState>> { event ->
            event.getContentIfNotHandled()?.let {
                // Only proceed if the event has never been handled
                when (it) {
                    Loading -> displayLoadingState(getString(R.string.index_is_location_service_available_loading))
                    Available -> {
                        // Permission is granted and location service is running, get ultraviolet index
                        viewModel.getUltravioletIndex()
                    }
                    LocationServiceOff -> {
                        binding.lvChartLoadingView.hideView()
                        askUser(
                            R.string.location_service_off_title,
                            R.string.location_service_off_message,
                            OPEN_LOCATION_SETTINGS.name
                        )
                    }
                    // LocationServiceNoPermission should never reached, but it is also implemented, just in case :)
                    LocationServiceNoPermission -> displayFailureState(LocationServiceNoPermissionException(), GetLastLocation)
                    BackgroundLocationNoPermission -> throw IllegalStateException("No background location permission should never be received.")
                    is Error -> displayFailureState(it.throwable, GetLastLocation)
                }
            }
        })

        viewModel.ultravioletIndexSource.observe(viewLifecycleOwner, Observer<UltravioletIndexState> {
            when (it) {
                UltravioletIndexState.Loading -> displayLoadingState(getString(R.string.index_get_ultraviolet_index_loading))
                is UltravioletIndexState.Success -> displaySuccessState(it.currentUltravioletIndex)
                is UltravioletIndexState.Failure -> displayFailureState(it.throwable, GetUltravioletIndex)
            }
        })

        // Check whether the view model has data or not. If not, it is the first time that the screen is shown, so ask
        // for data
        if (viewModel.ultravioletIndexSource.value == null) {
            fetchLocationProviderAvailability()
        }
    }
    //endregion

    //region Location provider availability
    override fun isLocationProviderAvailable() {
        viewModel.isLocationProviderAvailable()
    }
    //endregion

    //region Display state
    private fun displayLoadingState(message: String) {
        binding.lvChartLoadingView.showView(message)
        binding.ccChartCombinedChart.visibility = View.GONE
        binding.evChartErrorView.visibility = View.GONE
    }

    private fun displaySuccessState(currentUltravioletIndex: CurrentUltravioletIndex) {
        binding.lvChartLoadingView.hideView()
        binding.ccChartCombinedChart.visibility = View.VISIBLE
        binding.evChartErrorView.visibility = View.GONE

        updateView(currentUltravioletIndex)
    }

    private fun updateView(currentUltravioletIndex: CurrentUltravioletIndex) {
        val data = CombinedData()

        data.setData(generateLineData(currentUltravioletIndex))
        data.setData(generateBarData(currentUltravioletIndex))
//        data.setValueTypeface(tfLight)

        //binding.ccChartCombinedChart.xAxis.axisMaximum = data.xMax + 0.25f

        binding.ccChartCombinedChart.data = data
        binding.ccChartCombinedChart.invalidate()
    }

    private fun displayFailureState(throwable: Throwable, retryAction: RetryAction) {
        val errorBundle = errorBundleBuilder.build(throwable, retryAction)
        binding.evChartErrorView.errorBundle = errorBundle
        if (throwable is UviNotComputableException) {
            // Overwrite default error message
            val stringId = errorBundle.stringId
            val numberFormatter = NumberFormatHelper.getLocalNumberFormatter()
            val timeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyy HH:mm")

            binding.evChartErrorView.setErrorMessage(
                getString(
                    stringId,
                    numberFormatter.format(throwable.latitude),
                    numberFormatter.format(throwable.longitude),
                    throwable.sunriseZonedDateTime.zone.id,
                    throwable.sunriseZonedDateTime.format(timeFormatter),
                    throwable.sunsetZonedDateTime.format(timeFormatter)
                )
            )
        }

        binding.lvChartLoadingView.hideView()
        binding.ccChartCombinedChart.visibility = View.GONE
        binding.evChartErrorView.visibility = View.VISIBLE

        if (throwable is NoLocationAvailableException) {
            askUser(
                R.string.location_not_available_title,
                R.string.location_not_available_message,
                OPEN_GOOGLE_MAPS.name
            )
        }
    }
    //endregion

    //region Error View
    private val errorListener = object : ErrorListener {
        override fun onRetry(retryAction: RetryAction) {
            when (retryAction) {
                GetLastLocation -> fetchLocationProviderAvailability()
                GetUltravioletIndex -> viewModel.getUltravioletIndex()
                RefreshWidget -> throw IllegalStateException("Refresh widget is not supported")
            }
        }
    }

    private fun initializeErrorView() {
        errorBundleBuilder = IndexErrorBundleBuilder()
        binding.evChartErrorView.errorListener = errorListener
    }
    //endregion

    //region Chart data
    private fun generateBarData(currentUltravioletIndex: CurrentUltravioletIndex): BarData {
        val entries = ArrayList<BarEntry>()
        val colors = ArrayList<Int>()

        for ((index, value) in currentUltravioletIndex.uvi.withIndex()) {
            val roundedIndex = CurrentUltravioletIndex.fromValueToIndex(value)
            roundedIndex?.let { uvi ->
                if (0 < uvi) {
                    // Set index
                    entries.add(BarEntry(index.toFloat(), uvi.toFloat()))
                    // Set color
                    colors.add(ResourcesCompat.getColor(resources, ResourceUtils.fromUltravioletIndexToColorResourceId(uvi), null))
                }
            } ?: Timber.e("A negative ultraviolet index was found: $value")
        }

        val barDataSet = BarDataSet(entries, "Index")
        //val color = ResourcesCompat.getColor(resources, R.color.uvi_background, null)
        barDataSet.colors = colors
        barDataSet.setValueTextColors(colors)
        barDataSet.valueTextSize = 10f
        barDataSet.axisDependency = YAxis.AxisDependency.LEFT

        //val groupSpace = 0.06f
        //val barSpace = 0.02f // x2 dataset
        //val barWidth = 0.45f // x2 dataset
        // (0.45 + 0.02) * 2 + 0.06 = 1.00 -> interval per "group"

        val barData = BarData(barDataSet)
        barData.setValueFormatter(IntValueFormatter())
        //d.barWidth = barWidth

        // make this BarData object grouped
        //d.groupBars(0f, groupSpace, barSpace) // start at x = 0

        return barData
    }

    private fun generateLineData(currentUltravioletIndex: CurrentUltravioletIndex): LineData {
        val lineData = LineData()

        val entries = ArrayList<Entry>()

        for ((index, value) in currentUltravioletIndex.uvi.withIndex()) {
            if (0.0 < value) {
                entries.add(Entry(index.toFloat(), MathUtils.customRound(value, 1).toFloat()))
            }
        }

        val lineDataSet = LineDataSet(entries, "Value")
        val color = ResourcesCompat.getColor(resources, R.color.uvi_line, null)
        lineDataSet.color = color
        lineDataSet.lineWidth = 2.5f
        lineDataSet.setCircleColor(color)
        lineDataSet.circleRadius = 5f
        lineDataSet.fillColor = color
        lineDataSet.mode = LineDataSet.Mode.CUBIC_BEZIER
        lineDataSet.setDrawValues(true)
        lineDataSet.valueTextSize = 10f
        lineDataSet.valueTextColor = color

        lineDataSet.axisDependency = YAxis.AxisDependency.LEFT
        lineData.addDataSet(lineDataSet)
        lineData.setValueFormatter(FloatValueFormatter())

        return lineData
    }
    //endregion
}
