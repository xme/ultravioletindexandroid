/*
 * Copyright (C) 2019 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.app.information.markwon

import android.content.Context
import android.text.Spanned
import io.noties.markwon.Markwon
import io.reactivex.rxjava3.core.Single
import timber.log.Timber
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader

object MarkdownHelper {

    @JvmStatic
    fun render(markwon: Markwon, markdownText: String): Single<Spanned> {
        return Single.create { singleEmitter ->
            try {
                val text = markwon.toMarkdown(markdownText)
                if (!singleEmitter.isDisposed) {
                    singleEmitter.onSuccess(text)
                }
            } catch (e: IOException) {
                Timber.e(e, "An error was found while rendering markdown: ${e.message}")
                singleEmitter.tryOnError(e)
            }
        }
    }

    @JvmStatic
    fun load(context: Context): Single<String> {
        return Single.create { singleEmitter ->
            try {
                val stream = context.assets.open("README.md")
                val text = readStream(stream)
                if (!singleEmitter.isDisposed) {
                    singleEmitter.onSuccess(text)
                }
            } catch (e: IOException) {
                Timber.e(e, "An error was found while loading markdown: ${e.message}")
                singleEmitter.tryOnError(e)
            }
        }
    }

    @Throws(IOException::class)
    private fun readStream(inputStream: InputStream): String {
        val out: String
        var reader: BufferedReader? = null

        try {
            reader = BufferedReader(InputStreamReader(inputStream))
            val builder = StringBuilder()
            var line = reader.readLine()
            while (line != null) {
                builder.append(line)
                    .append('\n')
                line = reader.readLine()
            }
            out = builder.toString()
        } catch (e: IOException) {
            throw e
        } finally {
            reader?.let {
                try {
                    it.close()
                } catch (e: IOException) {
                    // no op
                }
            }
        }

        return out
    }
}