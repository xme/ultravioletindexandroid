/*
 * Copyright (C) 2018 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.datasource.ultravioletindex.remote.openweather

import androidx.annotation.FloatRange
import com.xme.client.android.ultravioletindex.data.ultravioletindex.source.UltravioletIndexDataStore
import com.xme.client.android.ultravioletindex.datasource.remote.errorhandling.RetrofitRxAdapterExceptionMapper
import com.xme.client.android.ultravioletindex.datasource.ultravioletindex.remote.openweather.mapper.OneCallResponseMapper
import com.xme.client.android.ultravioletindex.model.ultravioletindex.CurrentUltravioletIndex
import io.reactivex.rxjava3.core.Single

/**
 * Remote implementation of [UltravioletIndexDataStore] for retrieving [CurrentUltravioletIndex]
 * from the cloud with <a href="https://openweathermap.org/">Open Weather</a> provider.
 */
class OpenWeatherDataStore : UltravioletIndexDataStore {

    private val openWeatherRemoteApi by lazy {
        OpenWeatherRetrofitApi.create()
    }

    override fun getUltravioletIndex(
        @FloatRange(from = -90.0, to = 90.0) latitude: Double,
        @FloatRange(from = -180.0, to = 180.0) longitude: Double
    ): Single<CurrentUltravioletIndex> {
        val oneCallResponseMapper = OneCallResponseMapper()

        return openWeatherRemoteApi.getWeather(latitude, longitude)
            .map { response ->
                oneCallResponseMapper.transform(response)
            }
            .onErrorResumeNext { throwable ->
                // Map Retrofit exceptions to custom exceptions to avoid coupling the entire app to Retrofit
                Single.error(RetrofitRxAdapterExceptionMapper.getException(throwable))
            }

//        // Old OpenWeather API
//        // Current weather and UV index calls are independent, so zip operator can be used.
//        // If second call depends on first call, use the flatMap version.
//        // See: https://stackoverflow.com/questions/48250671/error-when-changed-lambda-for-flatmap-of-two-observables-to-flatmap-of-two-singl
//        return Single.zip(
//            openWeatherRemoteApi.getCurrentWeather(latitude, longitude),
//            openWeatherRemoteApi.getCurrentUv(latitude, longitude),
//            BiFunction<OpenWeatherCurrentWeather, OpenWeatherCurrentUltravioletIndex, CurrentUltravioletIndex>
//            { openWeatherCurrentWeatherResponse, openWeatherCurrentUltravioletIndexResponse ->
//                openWeatherCurrentUvResponseMapper.transform(
//                    openWeatherCurrentWeatherResponse,
//                    openWeatherCurrentUltravioletIndexResponse
//                )
//            })
//            .onErrorResumeNext { throwable ->
//                // Map Retrofit exceptions to custom exceptions to avoid coupling the entire app to Retrofit
//                Single.error(RetrofitRxAdapterExceptionMapper.getException(throwable))
//            }

//      // Old version ported from Java
//      openWeatherApi.getCurrentWeather(OPEN_WEATHER_APP_ID, latitude, longitude)
//            .flatMap { openWeatherCurrentWeatherResponse: OpenWeatherCurrentWeather ->
//                openWeatherApi.getCurrentUv(
//                    OPEN_WEATHER_APP_ID,
//                    latitude,
//                    longitude
//                )
//                    .map { openWeatherCurrentUltravioletIndexResponse: OpenWeatherCurrentUltravioletIndex ->
//                        openWeatherCurrentUvResponseMapper.transform(
//                            openWeatherCurrentWeatherResponse,
//                            openWeatherCurrentUltravioletIndexResponse
//                        )
//                    }
//            }

//            // Alternative flatMap versions for old Java port
//            // FlatMap with BiFunction is only for Observables, it is not available for Singles
//            // See: https://stackoverflow.com/questions/42696406/rxjava2-passing-data-through-flatmap-on-a-single
//            .flatMap(
//                { openWeatherCurrentWeatherResponse: OpenWeatherCurrentWeather ->
//                    openWeatherApi.getCurrentUv(
//                        OPEN_WEATHER_APP_ID,
//                        latitude,
//                        longitude
//                    )
//                }
//                // Specifying BiFunction
//                ,BiFunction <OpenWeatherCurrentWeather, OpenWeatherCurrentUltravioletIndex, CurrentUltravioletIndex> { openWeatherCurrentWeatherResponse, openWeatherCurrentUltravioletIndexResponse ->
//                    openWeatherCurrentUvResponseMapper.transform(
//                        openWeatherCurrentWeatherResponse,
//                        openWeatherCurrentUltravioletIndexResponse
//                    )
//
//                }
//                // Just using specifying types
//                ,{ openWeatherCurrentWeatherResponse: OpenWeatherCurrentWeather, openWeatherCurrentUltravioletIndexResponse: OpenWeatherCurrentUltravioletIndex ->
//                    openWeatherCurrentUvResponseMapper.transform(
//                        openWeatherCurrentWeatherResponse,
//                        openWeatherCurrentUltravioletIndexResponse
//                    )
//                }
//            )
    }
}
