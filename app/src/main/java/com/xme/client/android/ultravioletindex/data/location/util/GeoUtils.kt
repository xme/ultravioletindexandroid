/*
 * Copyright (C) 2018 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.data.location.util

import kotlin.math.atan2
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt

object GeoUtils {

    private const val EARTH_RADIUS = 6371000.0

    // See: https://www.movable-type.co.uk/scripts/latlong.html
    //    var R = 6371e3; // metres
    //    var φ1 = lat1.toRadians();
    //    var φ2 = lat2.toRadians();
    //    var Δφ = (lat2-lat1).toRadians();
    //    var Δλ = (lon2-lon1).toRadians();
    //
    //    var a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
    //        Math.cos(φ1) * Math.cos(φ2) *
    //            Math.sin(Δλ/2) * Math.sin(Δλ/2);
    //    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    //
    //    var d = R * c;
    @JvmStatic
    fun computeDistance(latitude1: Double, longitude1: Double, latitude2: Double, longitude2: Double): Double {
        val φ1 = Math.toRadians(latitude1)
        val φ2 = Math.toRadians(latitude2)
        val Δφ = Math.toRadians(latitude2 - latitude1)
        val Δλ = Math.toRadians(longitude2 - longitude1)

        val a = sin(Δφ / 2.0) * sin(Δφ / 2.0) + cos(φ1) * cos(φ2) * sin(Δλ / 2.0) * sin(Δλ / 2.0)
        val c = 2.0 * atan2(sqrt(a), sqrt(1.0 - a))

        return EARTH_RADIUS * c
    }
}