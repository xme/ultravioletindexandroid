/*
 * Copyright (C) 2019 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.app.information.markwon.gif;

import android.content.Context;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import com.xme.client.android.ultravioletindex.R;
import io.noties.markwon.AbstractMarkwonPlugin;
import io.noties.markwon.MarkwonConfiguration;
import io.noties.markwon.MarkwonSpansFactory;
import io.noties.markwon.RenderProps;
import io.noties.markwon.SpanFactory;
import io.noties.markwon.image.AsyncDrawableSpan;
import io.noties.markwon.image.ImageProps;
import java.util.Objects;
import org.commonmark.node.Image;

public class GifAwarePlugin extends AbstractMarkwonPlugin {

    @NonNull
    public static GifAwarePlugin create(@NonNull Context context) {
        return new GifAwarePlugin(context);
    }

    private final Context context;
    private final GifProcessor processor;

    GifAwarePlugin(@NonNull Context context) {
        this.context = context;
        this.processor = GifProcessor.create();
    }

    @Override
    public void configureSpansFactory(@NonNull MarkwonSpansFactory.Builder builder) {

        final GifPlaceholder gifPlaceholder = new GifPlaceholder(
                Objects.requireNonNull(ContextCompat.getDrawable(context, R.drawable.ic_play_circle_filled_18dp_white)),
                0x20000000
        );

        builder.setFactory(Image.class, new SpanFactory() {
            @Override
            public Object getSpans(@NonNull MarkwonConfiguration configuration, @NonNull RenderProps props) {
                return new AsyncDrawableSpan(
                        configuration.theme(),
                        new GifAwareAsyncDrawable(
                                gifPlaceholder,
                                ImageProps.DESTINATION.require(props),
                                configuration.asyncDrawableLoader(),
                                configuration.imageSizeResolver(),
                                ImageProps.IMAGE_SIZE.get(props)
                        ),
                        AsyncDrawableSpan.ALIGN_BOTTOM,
                        ImageProps.REPLACEMENT_TEXT_IS_LINK.get(props, false)
                );
            }
        });
    }

    @Override
    public void afterSetText(@NonNull TextView textView) {
        processor.process(textView);
    }
}
