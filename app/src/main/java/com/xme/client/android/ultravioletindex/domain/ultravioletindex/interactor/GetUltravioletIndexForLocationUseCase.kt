/*
 * Copyright (C) 2018 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.domain.ultravioletindex.interactor

import androidx.annotation.FloatRange
import com.xme.client.android.ultravioletindex.domain.interactor.SingleUseCase
import com.xme.client.android.ultravioletindex.domain.ultravioletindex.repository.UltravioletIndexRepository
import com.xme.client.android.ultravioletindex.model.ultravioletindex.CurrentUltravioletIndex
import io.reactivex.rxjava3.core.Single

/**
 * Use case for retrieving current ultraviolet index from the [UltravioletIndexRepository] for a given location.
 */
class GetUltravioletIndexForLocationUseCase(private val ultravioletIndexRepository: UltravioletIndexRepository) :
    SingleUseCase<CurrentUltravioletIndex, GetUltravioletIndexForLocationUseCase.Params?>() {

    class Params private constructor(val latitude: Double, val longitude: Double) {
        companion object {
            @JvmStatic
            fun withLocation(
                @FloatRange(from = -90.0, to = 90.0)
                latitude: Double,
                @FloatRange(from = -180.0, to = 180.0)
                longitude: Double
            ): Params {
                return Params(
                    latitude,
                    longitude
                )
            }
        }
    }

    override fun buildUseCaseObservable(params: Params?): Single<CurrentUltravioletIndex> {
        requireNotNull(params) { "Get ultraviolet index cannot be called with null params" }
        return ultravioletIndexRepository.getUltravioletIndex(params.latitude, params.longitude)
    }
}