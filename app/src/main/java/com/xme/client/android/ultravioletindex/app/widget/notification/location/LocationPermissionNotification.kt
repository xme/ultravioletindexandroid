/*
 * Copyright (C) 2018 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.app.widget.notification.location

import android.content.Context
import androidx.core.app.NotificationCompat
import com.xme.client.android.ultravioletindex.R
import com.xme.client.android.ultravioletindex.app.notification.BigStyleNotificationData
import com.xme.client.android.ultravioletindex.app.notification.ChannelNotificationData

class LocationPermissionNotification(
    override val smallIcon: Int,
    override val contentTitle: String,
    override val contentText: String,
    override val priority: Int,

    override val bigContentTitle: String,
    override val bigContentText: String,

    override val channelId: String,
    override val channelName: CharSequence,
    override val channelDescription: String,
    override val channelImportance: Int,
    override val isChannelEnableVibrate: Boolean,
    override val isChannelEnableLight: Boolean,
    override val channelLightColor: Int,
    override val channelLockScreenVisibility: Int
) : BigStyleNotificationData, ChannelNotificationData {

    companion object {
        @JvmStatic
        fun buildNoLocationPermissionNotification(context: Context): LocationPermissionNotification {
            return LocationPermissionNotification(
                R.drawable.ic_sun,
                context.getString(R.string.notification_location_permission_title),
                context.getString(R.string.notification_location_permission_message),
                NotificationCompat.PRIORITY_DEFAULT,
                context.getString(R.string.notification_location_permission_big_title),
                context.getString(R.string.notification_location_permission_big_message),
                LocationNotificationChannel.channelId,
                context.getString(LocationNotificationChannel.channelName),
                context.getString(LocationNotificationChannel.channelDescription),
                LocationNotificationChannel.importance,
                LocationNotificationChannel.isChannelEnableVibrate,
                LocationNotificationChannel.isChannelEnableLight,
                LocationNotificationChannel.channelLightColor,
                LocationNotificationChannel.channelLockScreenVisibility
            )
        }

        @JvmStatic
        fun buildNoBackgroundLocationPermissionNotification(context: Context): LocationPermissionNotification {
            return LocationPermissionNotification(
                R.drawable.ic_sun,
                context.getString(R.string.notification_background_location_permission_title),
                context.getString(R.string.notification_background_location_permission_message),
                NotificationCompat.PRIORITY_DEFAULT,
                context.getString(R.string.notification_background_location_permission_big_title),
                context.getString(R.string.notification_background_location_permission_big_message),
                LocationNotificationChannel.channelId,
                context.getString(LocationNotificationChannel.channelName),
                context.getString(LocationNotificationChannel.channelDescription),
                LocationNotificationChannel.importance,
                LocationNotificationChannel.isChannelEnableVibrate,
                LocationNotificationChannel.isChannelEnableLight,
                LocationNotificationChannel.channelLightColor,
                LocationNotificationChannel.channelLockScreenVisibility
            )
        }
    }
}
