/*
 * Copyright (C) 2018 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.app.widget

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.AppWidgetTarget
import com.xme.client.android.ultravioletindex.R
import com.xme.client.android.ultravioletindex.app.common.state.LocationServiceState
import com.xme.client.android.ultravioletindex.app.common.state.UltravioletIndexState
import com.xme.client.android.ultravioletindex.app.common.state.UltravioletIndexState.Failure
import com.xme.client.android.ultravioletindex.app.common.state.UltravioletIndexState.Loading
import com.xme.client.android.ultravioletindex.app.common.state.UltravioletIndexState.Success
import com.xme.client.android.ultravioletindex.app.common.util.ResourceUtils
import com.xme.client.android.ultravioletindex.app.common.util.UltravioletImageContents
import com.xme.client.android.ultravioletindex.app.main.MainActivity
import com.xme.client.android.ultravioletindex.app.widget.notification.location.LocationPermissionNotification
import com.xme.client.android.ultravioletindex.app.widget.notification.location.LocationServiceNotification
import com.xme.client.android.ultravioletindex.app.widget.notification.location.WidgetNotificationUtils
import com.xme.client.android.ultravioletindex.model.ultravioletindex.CurrentUltravioletIndex
import timber.log.Timber

class UltravioletWidgetView {

    companion object {
        private const val LOCATION_PERMISSION_NOTIFICATION_ID = 0
        private const val BACKGROUND_LOCATION_PERMISSION_NOTIFICATION_ID = 1
        private const val LOCATION_SERVICE_NOTIFICATION_ID = 2

        /**
         * Updates ultraviolet index widget view with a [LocationServiceState].
         */
        @JvmStatic
        fun updateWidgetViews(context: Context, appWidgetId: Int, locationServiceState: LocationServiceState) {
            // Get state drawable
            val ultravioletImageContents = when (locationServiceState) {
                LocationServiceState.Loading -> UltravioletImageContents(R.drawable.ic_uvi_loading, context.getString(R.string.loading))
                LocationServiceState.Available -> {
                    Timber.e("Allowed result should never be passed to the widget view")
                    UltravioletImageContents(R.drawable.ic_uvi_error, context.getString(R.string.uvi_error))
                }
                LocationServiceState.Failure.LocationServiceNoPermission -> UltravioletImageContents(R.drawable.ic_location_service_locked, context.getString(R.string.uvi_error_no_location_permission))
                LocationServiceState.Failure.BackgroundLocationNoPermission -> UltravioletImageContents(R.drawable.ic_location_service_locked, context.getString(R.string.uvi_error_no_background_location_permission))
                LocationServiceState.Failure.LocationServiceOff -> UltravioletImageContents(R.drawable.ic_location_service_off, context.getString(R.string.uvi_error_location_service_off))
                is LocationServiceState.Failure.Error -> UltravioletImageContents(R.drawable.ic_uvi_error, context.getString(R.string.uvi_error))
            }

            updateWidgetViews(context, appWidgetId, ultravioletImageContents)
        }

        /**
         * Updates ultraviolet index widget view with an [UltravioletIndexState].
         */
        @JvmStatic
        fun updateWidgetViews(context: Context, appWidgetId: Int, ultravioletIndexState: UltravioletIndexState) {
            // Get state drawable
            val ultravioletImageContents = when (ultravioletIndexState) {
                Loading -> UltravioletImageContents(R.drawable.ic_uvi_loading, context.getString(R.string.loading))
                is Success -> {
                    val uvi = CurrentUltravioletIndex.fromValueToIndex(ultravioletIndexState.currentUltravioletIndex.currentUltravioletIndex)
                    ResourceUtils.fromUltravioletIndexToImageContents(context, uvi)
                }
                is Failure -> UltravioletImageContents(R.drawable.ic_uvi_error, context.getString(R.string.uvi_error))
            }

            updateWidgetViews(context, appWidgetId, ultravioletImageContents)
        }

        /**
         * Updates ultraviolet index widget view.
         */
        @JvmStatic
        private fun updateWidgetViews(context: Context, appWidgetId: Int, ultravioletImageContents: UltravioletImageContents) {
            val widgetView = RemoteViews(context.packageName, R.layout.ultraviolet_widget)

            // Set an Intent to launch MainActivity when clicking on the widget
            val intent = Intent(context, MainActivity::class.java)
            val pendingIntent = PendingIntent.getActivity(context, 0, intent, 0)
            widgetView.setOnClickPendingIntent(R.id.rl_ultraviolet_widget_layout, pendingIntent)

            // Set image
            val appWidgetTarget = AppWidgetTarget(context, R.id.iv_ultraviolet_widget_image, widgetView, appWidgetId)
            Glide
                .with(context)
                .asBitmap() // This is needed because AppWidgetTarget is a Bitmap target. See: https://github.com/bumptech/glide/issues/2717#issuecomment-351791721
                .transition(BitmapTransitionOptions.withCrossFade())
                .load(ultravioletImageContents.drawableId)
                .listener(object : RequestListener<Bitmap> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: com.bumptech.glide.request.target.Target<Bitmap>?, isFirstResource: Boolean): Boolean {
                        Timber.e(e, "Drawable cannot be loaded and set in widget image view")
                        return false // Important to return false so the error placeholder can be placed
                    }

                    override fun onResourceReady(resource: Bitmap?, model: Any?, target: com.bumptech.glide.request.target.Target<Bitmap>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        // Set contentDescription in order to enable accessibility and be usable with TalkBack
                        widgetView.setContentDescription(R.id.iv_ultraviolet_widget_image, ultravioletImageContents.contentDescription)

                        pushWidgetUpdate(context, appWidgetId, widgetView)

                        return false
                    }
                })
                .into(appWidgetTarget)
        }

        /**
         * Pushes update for a given widget.
         */
        @JvmStatic
        fun pushWidgetUpdate(context: Context, appWidgetId: Int, remoteViews: RemoteViews) {
            val manager = AppWidgetManager.getInstance(context)
            // Remember to check always for null in platform types (types ended in !).
            // See: https://stackoverflow.com/a/43826700/5189200
            manager?.let {
                manager.updateAppWidget(appWidgetId, remoteViews)
                Timber.d("Widget %d was updated", appWidgetId)
            }
        }

        /**
         * Shows a notification for explaining that location permission is not granted.
         */
        @JvmStatic
        fun showNoLocationPermissionNotification(context: Context) {
            val noLocationPermissionNotification = LocationPermissionNotification.buildNoLocationPermissionNotification(context)
            val notificationBuilder = buildNotificationBuilder(context, noLocationPermissionNotification)

            with(NotificationManagerCompat.from(context)) {
                // The notification id is a unique int for each notification that you must define.
                // In this case, it is always the same number to overwrite the notification and do
                // not overwhelm the user if one or more widget do not have access to location
                // service.
                notify(LOCATION_PERMISSION_NOTIFICATION_ID, notificationBuilder.build())
            }
        }

        /**
         * Shows a notification for explaining that background location permission is not granted.
         */
        @JvmStatic
        fun showNoBackgroundLocationPermissionNotification(context: Context) {
            val noBackgroundLocationPermissionNotification = LocationPermissionNotification.buildNoBackgroundLocationPermissionNotification(context)
            val notificationBuilder = buildNotificationBuilder(context, noBackgroundLocationPermissionNotification)

            with(NotificationManagerCompat.from(context)) {
                notify(BACKGROUND_LOCATION_PERMISSION_NOTIFICATION_ID, notificationBuilder.build())
            }
        }

        /**
         * Shows a notification for explaining that location service is off.
         */
        @JvmStatic
        fun showLocationServiceNotification(context: Context) {
            val locationServiceNotification = LocationServiceNotification.buildLocationServiceNotification(context)
            val notificationBuilder = buildNotificationBuilder(context, locationServiceNotification)

            with(NotificationManagerCompat.from(context)) {
                notify(LOCATION_SERVICE_NOTIFICATION_ID, notificationBuilder.build())
            }
        }

        /**
         * Shows a notification for explaining that location service is off.
         */
        @JvmStatic
        fun removeLocationNotifications(context: Context) {
            val notificationManager = NotificationManagerCompat.from(context)
            notificationManager.run {
                cancel(LOCATION_SERVICE_NOTIFICATION_ID)
                cancel(LOCATION_PERMISSION_NOTIFICATION_ID)
                cancel(BACKGROUND_LOCATION_PERMISSION_NOTIFICATION_ID)
            }
        }

        private fun buildNotificationBuilder(context: Context, locationPermissionNotification: LocationPermissionNotification): NotificationCompat.Builder {
            return WidgetNotificationUtils.buildNotificationBuilder(
                context,
                locationPermissionNotification.bigContentTitle,
                locationPermissionNotification.bigContentText,
                locationPermissionNotification.channelId,
                locationPermissionNotification.smallIcon,
                locationPermissionNotification.contentTitle,
                locationPermissionNotification.contentText,
                locationPermissionNotification.priority
            )
        }

        private fun buildNotificationBuilder(context: Context, locationServiceNotification: LocationServiceNotification): NotificationCompat.Builder {
            return WidgetNotificationUtils.buildNotificationBuilder(
                context,
                locationServiceNotification.bigContentTitle,
                locationServiceNotification.bigContentText,
                locationServiceNotification.channelId,
                locationServiceNotification.smallIcon,
                locationServiceNotification.contentTitle,
                locationServiceNotification.contentText,
                locationServiceNotification.priority
            )
        }
    }
}