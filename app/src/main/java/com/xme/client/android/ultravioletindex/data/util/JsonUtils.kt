/*
 * Copyright (C) 2018 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.data.util

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonIOException
import com.google.gson.JsonSyntaxException
import timber.log.Timber
import java.lang.reflect.Type

// See: https://stackoverflow.com/questions/51811391/utils-classes-in-kotlin/51811424#51811424
object JsonUtils {

    private val jsonManager: Gson
        get() = GsonBuilder().setPrettyPrinting()
            //.setDateFormat( UltravioletIndexApiConstants.DATE_ISO_FORMAT )
            //.registerTypeAdapter(Date.class, new UTCDateAdapter()) // Used to create dates in UTC format instead of defaulting to local time
            .create()

    @JvmStatic
    @Throws(JsonIOException::class)
    fun fromPojoToJson(entity: Any): String {
        val gson = jsonManager
        val result: String
        try {
            result = gson.toJson(entity)
        } catch (e: JsonIOException) {
            Timber.e("fromPojoToJson::POJO to JSON conversion has failed using class: ${entity.javaClass.name}")
            throw e
        }

        return result
    }

    @JvmStatic
    @Throws(JsonSyntaxException::class, UnsupportedOperationException::class)
    fun <T> fromJsonToPojo(json: String, classOfT: Class<T>): T? {
        val gson = jsonManager
        val pojo: T?
        try {
            pojo = gson.fromJson(json, classOfT)
        } catch (e: JsonSyntaxException) {
            Timber.e("fromJsonToPojo::JSON to POJO conversion has failed due to a syntax exception using class: ${classOfT.name}")
            throw e
        } catch (e: UnsupportedOperationException) {
            Timber.e("fromJsonToPojo::JSON to POJO conversion has failed due to a unsupported operation exception using class: ${classOfT.name}")
            throw e
        }

        return pojo
    }

    @JvmStatic
    @Throws(JsonSyntaxException::class, UnsupportedOperationException::class)
    fun <T> fromJsonToPojo(json: String, typeOfT: Type): T? {
        val gson = jsonManager
        val pojo: T?
        try {
            pojo = gson.fromJson<T>(json, typeOfT)
        } catch (e: JsonSyntaxException) {
            Timber.e("fromJsonToPojo::JSON to POJO conversion has failed due to a syntax exception using class: $typeOfT")
            throw e
        } catch (e: UnsupportedOperationException) {
            Timber.e("fromJsonToPojo::JSON to POJO conversion has failed due to a unsupported operation exception using class: $typeOfT")
            throw e
        }

        return pojo
    }
}
