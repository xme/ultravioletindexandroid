package com.xme.client.android.ultravioletindex.datasource.ultravioletindex.remote.openweather.mapper

import com.xme.client.android.ultravioletindex.datasource.ultravioletindex.remote.openweather.model.OneCall
import com.xme.client.android.ultravioletindex.model.ultravioletindex.CurrentUltravioletIndex

class OneCallResponseMapper {

    fun transform(oneCall: OneCall): CurrentUltravioletIndex {
        return CurrentUltravioletIndex(
            oneCall.lat,
            oneCall.lon,
            oneCall.current.dt,
            oneCall.daily[0].uvi,
            oneCall.current.sunrise,
            oneCall.current.sunset,
            oneCall.timezone
        )
    }
}