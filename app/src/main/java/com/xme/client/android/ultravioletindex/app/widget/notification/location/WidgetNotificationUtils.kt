/*
 * Copyright (C) 2020 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.app.widget.notification.location

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.annotation.DrawableRes
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.xme.client.android.ultravioletindex.R
import com.xme.client.android.ultravioletindex.app.main.MainActivity

object WidgetNotificationUtils {

    @JvmStatic
    fun buildNotificationBuilder(
        context: Context,
        bigContentTitle: String,
        bigContentText: String,
        channelId: String,
        @DrawableRes smallIcon: Int,
        contentTitle: String,
        contentText: String,
        priority: Int
    ): NotificationCompat.Builder {
        // Create an explicit intent for an activity in your app
        val intent = Intent(context, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        // Use PendingIntent.getActivity() to start a new activity with the pending intent
        val pendingIntent: PendingIntent = PendingIntent.getActivity(context, 0, intent, 0)

        // TODO: Android 10 does not use bold text with big text style. Styling should be done using HTML. See: https://developer.android.com/training/notify-user/expanded#inbox-style
        val bigTextStyle = NotificationCompat.BigTextStyle()
            // Overrides ContentTitle in the big form of the template
            .setBigContentTitle(bigContentTitle)
            // Overrides ContentText in the big form of the template
            .bigText(bigContentText)

        return NotificationCompat.Builder(context, channelId)
            .setSmallIcon(smallIcon)
            .setColor(ContextCompat.getColor(context, R.color.uvi_high))
            .setContentTitle(contentTitle)
            .setContentText(contentText)
            .setStyle(bigTextStyle)
            .setPriority(priority)
            // Set the intent that will fire when the user taps the notification
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
    }
}