/*
 * Copyright (C) 2019 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.app.index

import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.browser.customtabs.CustomTabsIntent
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.transition.DrawableCrossFadeFactory
import com.xme.client.android.ultravioletindex.BuildConfig
import com.xme.client.android.ultravioletindex.R
import com.xme.client.android.ultravioletindex.app.common.PRIVACY_URL
import com.xme.client.android.ultravioletindex.app.common.TERMS_URL
import com.xme.client.android.ultravioletindex.app.common.error.ErrorBundleBuilder
import com.xme.client.android.ultravioletindex.app.common.error.RetryAction
import com.xme.client.android.ultravioletindex.app.common.error.RetryAction.GetLastLocation
import com.xme.client.android.ultravioletindex.app.common.error.RetryAction.GetUltravioletIndex
import com.xme.client.android.ultravioletindex.app.common.error.RetryAction.RefreshWidget
import com.xme.client.android.ultravioletindex.app.common.event.Event
import com.xme.client.android.ultravioletindex.app.common.exception.LocationServiceNoPermissionException
import com.xme.client.android.ultravioletindex.app.common.internationalization.NumberFormatHelper
import com.xme.client.android.ultravioletindex.app.common.state.LocationServiceState
import com.xme.client.android.ultravioletindex.app.common.state.LocationServiceState.Available
import com.xme.client.android.ultravioletindex.app.common.state.LocationServiceState.Failure.BackgroundLocationNoPermission
import com.xme.client.android.ultravioletindex.app.common.state.LocationServiceState.Failure.Error
import com.xme.client.android.ultravioletindex.app.common.state.LocationServiceState.Failure.LocationServiceNoPermission
import com.xme.client.android.ultravioletindex.app.common.state.LocationServiceState.Failure.LocationServiceOff
import com.xme.client.android.ultravioletindex.app.common.state.LocationServiceState.Loading
import com.xme.client.android.ultravioletindex.app.common.state.UltravioletIndexState
import com.xme.client.android.ultravioletindex.app.common.util.ResourceUtils
import com.xme.client.android.ultravioletindex.app.common.view.error.ErrorListener
import com.xme.client.android.ultravioletindex.app.common.view.index.IndexBaseFragment
import com.xme.client.android.ultravioletindex.app.index.dialog.DialogAction.OPEN_GOOGLE_MAPS
import com.xme.client.android.ultravioletindex.app.index.dialog.DialogAction.OPEN_LOCATION_SETTINGS
import com.xme.client.android.ultravioletindex.app.index.error.IndexErrorBundleBuilder
import com.xme.client.android.ultravioletindex.app.widget.util.WidgetUtils
import com.xme.client.android.ultravioletindex.databinding.IndexFragmentBinding
import com.xme.client.android.ultravioletindex.model.location.exception.NoLocationAvailableException
import com.xme.client.android.ultravioletindex.model.ultravioletindex.CurrentUltravioletIndex
import com.xme.client.android.ultravioletindex.model.ultravioletindex.exception.UviNotComputableException
import timber.log.Timber
import java.time.format.DateTimeFormatter

class IndexFragment : IndexBaseFragment() {

    private var _binding: IndexFragmentBinding? = null
    private val binding get() = _binding!! // This property is only valid between onCreateView and onDestroyView

    private val viewModel: IndexViewModel by activityViewModels() // Share view model between index and chart fragments
    private lateinit var errorBundleBuilder: ErrorBundleBuilder

    //region Life cycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = IndexFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun initializeViews(savedInstanceState: Bundle?) {
        super.initializeViews(savedInstanceState)

        binding.tvIndexNoContents.visibility = View.VISIBLE
        binding.llIndexContentLayout.visibility = View.GONE

        initializeErrorView()

        if (BuildConfig.DEBUG) {
            binding.btIndexDebugButton.setOnClickListener {
                findNavController().navigate(R.id.action_index_fragment_to_debug_fragment)
            }
        } else {
            binding.btIndexDebugButton.visibility = View.GONE
        }
    }

    override fun initializeContents(savedInstanceState: Bundle?) {
        super.initializeContents(savedInstanceState)

        // Listen to view model events published by the dialogs shown in this fragment
        listenToGenericDialog()

        // Link the fragment and the view model with "viewLifecycleOwner", so that observers
        // can be subscribed in onActivityCreated() and can be automatically unsubscribed
        // in onDestroyView().
        // IMPORTANT: Never use "this" as lifecycle owner.
        // See: https://medium.com/@BladeCoder/architecture-components-pitfalls-part-1-9300dd969808
        viewModel.isLocationProviderAvailableEvent.observe(viewLifecycleOwner, Observer<Event<LocationServiceState>> { event ->
            event.getContentIfNotHandled()?.let {
                // Only proceed if the event has never been handled
                when (it) {
                    Loading -> displayLoadingState(getString(R.string.index_is_location_service_available_loading))
                    Available -> {
                        // Permission is granted and location service is running, get ultraviolet index
                        viewModel.getUltravioletIndex()
                    }
                    LocationServiceOff -> {
                        binding.lvIndexLoadingView.hideView()
                        askUser(
                            R.string.location_service_off_title,
                            R.string.location_service_off_message,
                            OPEN_LOCATION_SETTINGS.name
                        )
                    }
                    // LocationServiceNoPermission should never reached, but it is also implemented, just in case :)
                    LocationServiceNoPermission -> displayFailureState(LocationServiceNoPermissionException(), GetLastLocation)
                    BackgroundLocationNoPermission -> throw IllegalStateException("No background location permission should never be received.")
                    is Error -> displayFailureState(it.throwable, GetLastLocation)
                }
            }
        })

        viewModel.ultravioletIndexSource.observe(viewLifecycleOwner, Observer<UltravioletIndexState> {
            when (it) {
                UltravioletIndexState.Loading -> displayLoadingState(getString(R.string.index_get_ultraviolet_index_loading))
                is UltravioletIndexState.Success -> displaySuccessState(it.currentUltravioletIndex)
                is UltravioletIndexState.Failure -> displayFailureState(it.throwable, GetUltravioletIndex)
            }
        })

        // Check whether the view model has data or not. If not, it is the first time that the screen is shown, so ask
        // for data
        if (viewModel.ultravioletIndexSource.value == null) {
            // IMPORTANT: This approach may lead to problems with dialogs when using the navigation component, see
            // explanation at IndexBaseFragment::askUser function
            fetchLocationProviderAvailability()
        }
    }
    //endregion

    //region menu
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.index_menu, menu)

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.index_menu_privacy -> {
                openChromeCustomTab(PRIVACY_URL)
                true
            }
            R.id.index_menu_terms -> {
                openChromeCustomTab(TERMS_URL)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
    //endregion

    //region Location provider availability
    private fun openChromeCustomTab(url: String) {
        context?.let {
            val builder = CustomTabsIntent.Builder()
            val customTabsIntent = builder.build()
            customTabsIntent.launchUrl(it, Uri.parse(url))
        } ?: Timber.e("Context is null!!!")
    }
    //endregion

    //region Location provider availability
    override fun isLocationProviderAvailable() {
        viewModel.isLocationProviderAvailable()
    }
    //endregion

    //region Display state
    private fun displayLoadingState(message: String) {
        binding.lvIndexLoadingView.showView(message)
        binding.nsvIndexScroll.visibility = View.GONE
        binding.evIndexErrorView.visibility = View.GONE
    }

    private fun displaySuccessState(currentUltravioletIndex: CurrentUltravioletIndex) {
        binding.lvIndexLoadingView.hideView()
        binding.nsvIndexScroll.visibility = View.VISIBLE
        binding.tvIndexNoContents.visibility = View.GONE
        binding.llIndexContentLayout.visibility = View.VISIBLE
        binding.evIndexErrorView.visibility = View.GONE

        updateView(currentUltravioletIndex)
        refreshWidgets()
    }

    private fun updateView(currentUltravioletIndex: CurrentUltravioletIndex) {
        // Drawable
        val uvi = CurrentUltravioletIndex.fromValueToIndex(currentUltravioletIndex.currentUltravioletIndex)
        context?.let {
            val ultravioletImageContents = ResourceUtils.fromUltravioletIndexToImageContents(it, uvi)
            val factory = DrawableCrossFadeFactory.Builder().setCrossFadeEnabled(true).build()

            Glide.with(it)
                .load(ultravioletImageContents.drawableId)
                .transition(withCrossFade(factory))
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, isFirstResource: Boolean): Boolean {
                        Timber.e(e,"Drawable cannot be set loaded and set in image view")
                        return false // Important to return false so the error placeholder can be placed
                    }

                    override fun onResourceReady(resource: Drawable?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        // Set contentDescription in order to enable accessibility and be usable with TalkBack
                        binding.ivIndexUltravioletIndexImage.contentDescription = ultravioletImageContents.contentDescription
                        return false
                    }
                })
                .into(binding.ivIndexUltravioletIndexImage)
        } ?: Timber.e("Context is null")

        val numberFormatter = NumberFormatHelper.getLocalNumberFormatter()
        val timeFormatter = DateTimeFormatter.ofPattern("HH:mm")

        binding.tvIndexCurrentValueText.text = numberFormatter.format(currentUltravioletIndex.currentUltravioletIndex)
        binding.tvIndexMaxValueText.text = numberFormatter.format(currentUltravioletIndex.value)
        binding.tvIndexMaxValueTimeText.text = currentUltravioletIndex.valueZonedDateTime.format(timeFormatter)
        binding.tvIndexLatitudeText.text = numberFormatter.format(currentUltravioletIndex.latitude)
        binding.tvIndexLongitudeText.text = numberFormatter.format(currentUltravioletIndex.longitude)
        binding.tvIndexSunriseText.text = currentUltravioletIndex.sunriseZonedDateTime.format(timeFormatter)
        binding.tvIndexSunsetText.text = currentUltravioletIndex.sunsetZonedDateTime.format(timeFormatter)
        binding.tvIndexDateText.text = currentUltravioletIndex.valueZonedDateTime.format(DateTimeFormatter.ISO_LOCAL_DATE)
        binding.tvIndexTimeZoneText.text = currentUltravioletIndex.timeZoneId
    }

    private fun displayFailureState(throwable: Throwable, retryAction: RetryAction) {
        val errorBundle = errorBundleBuilder.build(throwable, retryAction)
        binding.evIndexErrorView.errorBundle = errorBundle
        if (throwable is UviNotComputableException) {
            // Overwrite default error message
            val stringId = errorBundle.stringId
            val numberFormatter = NumberFormatHelper.getLocalNumberFormatter()
            val timeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyy HH:mm")

            binding.evIndexErrorView.setErrorMessage(
                getString(
                    stringId,
                    numberFormatter.format(throwable.latitude),
                    numberFormatter.format(throwable.longitude),
                    throwable.sunriseZonedDateTime.zone.id,
                    throwable.sunriseZonedDateTime.format(timeFormatter),
                    throwable.sunsetZonedDateTime.format(timeFormatter)
                )
            )
        }

        binding.lvIndexLoadingView.hideView()
        binding.nsvIndexScroll.visibility = View.GONE
        binding.evIndexErrorView.visibility = View.VISIBLE

        if (throwable is NoLocationAvailableException) {
            askUser(
                R.string.location_not_available_title,
                R.string.location_not_available_message,
                OPEN_GOOGLE_MAPS.name
            )
        }

        refreshWidgets()
    }

    private fun refreshWidgets() {
        context?.let {
            Timber.d("Refreshing widgets")
            WidgetUtils.refreshWidgets(it)
        } ?: Timber.e("Context is null")
    }
    //endregion

    //region Error View
    private val errorListener = object : ErrorListener {
        override fun onRetry(retryAction: RetryAction) {
            when (retryAction) {
                GetLastLocation -> fetchLocationProviderAvailability()
                GetUltravioletIndex -> viewModel.getUltravioletIndex()
                RefreshWidget -> throw IllegalStateException("Refresh widget is not supported")
            }
        }
    }

    private fun initializeErrorView() {
        errorBundleBuilder = IndexErrorBundleBuilder()
        binding.evIndexErrorView.errorListener = errorListener
    }
    //endregion
}
