/*
 * Copyright (C) 2018 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.data.ultravioletindex

import androidx.annotation.FloatRange
import com.xme.client.android.ultravioletindex.data.ultravioletindex.source.UltravioletIndexDataStoreFactory
import com.xme.client.android.ultravioletindex.domain.ultravioletindex.repository.UltravioletIndexRepository
import com.xme.client.android.ultravioletindex.model.ultravioletindex.CurrentUltravioletIndex
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import timber.log.Timber

/**
 * Provides an implementation of the [UltravioletIndexRepository] interface for communicating to and from
 * data sources.
 */
class UltravioletIndexDataRepository(
    private val factory: UltravioletIndexDataStoreFactory
) : UltravioletIndexRepository {

    override fun getUltravioletIndex(
        @FloatRange(from = -90.0, to = 90.0) latitude: Double,
        @FloatRange(from = -180.0, to = 180.0) longitude: Double
    ): Single<CurrentUltravioletIndex> {
        return factory.retrieveCacheDataStore().isUltravioletIndexCached(latitude, longitude)
            .flatMap { isCached ->
                // Get data store based on whether cached data is valid or not
                val ultravioletIndexDataStore = factory.retrieveDataStore(isCached)
                // So the result is transparently obtained from local or remote
                var currentUltravioletIndexSource = ultravioletIndexDataStore.getUltravioletIndex(latitude, longitude)

                if (!isCached) {
                    // But, if result comes from remote, save it to cache before return it
                    Timber.d("Getting value from remote")
                    currentUltravioletIndexSource = currentUltravioletIndexSource
                        .flatMap { currentUltravioletIndex ->
                            saveUltravioletIndex(currentUltravioletIndex).toSingle { currentUltravioletIndex }
                        }
                } else {
                    Timber.d("Getting value from cache")
                }

                currentUltravioletIndexSource
            }
    }

    private fun saveUltravioletIndex(currentUltravioletIndex: CurrentUltravioletIndex): Completable {
        return factory.retrieveCacheDataStore().saveUltravioletIndex(currentUltravioletIndex)
    }

    private fun clearUltravioletIndex(): Completable {
        return factory.retrieveCacheDataStore().clearUltravioletIndex()
    }
}