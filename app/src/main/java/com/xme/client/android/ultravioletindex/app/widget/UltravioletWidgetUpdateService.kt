/*
 * Copyright (C) 2018 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.app.widget

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Handler
import android.os.Looper
import androidx.core.app.JobIntentService
import androidx.core.content.ContextCompat
import com.xme.client.android.ultravioletindex.app.common.LOCATION_PERMISSION
import com.xme.client.android.ultravioletindex.app.common.state.LocationServiceState
import com.xme.client.android.ultravioletindex.app.common.state.UltravioletIndexState
import com.xme.client.android.ultravioletindex.app.common.state.UltravioletIndexState.Failure
import com.xme.client.android.ultravioletindex.app.common.state.UltravioletIndexState.Success
import com.xme.client.android.ultravioletindex.domain.location.interactor.IsLocationProviderAvailableUseCase
import com.xme.client.android.ultravioletindex.domain.ultravioletindex.interactor.GetUltravioletIndexUseCase
import com.xme.client.android.ultravioletindex.injection.Injection
import com.xme.client.android.ultravioletindex.model.ultravioletindex.CurrentUltravioletIndex
import io.reactivex.rxjava3.observers.DisposableSingleObserver
import timber.log.Timber
import java.util.concurrent.CountDownLatch

/**
 * This service takes care of obtaining the data necessary for updating widgets in background.
 * Then, [UltravioletWidgetView] is called to update each view. This service can be seen as a
 * presenter in the Model-View-Presenter pattern.
 *
 * IMPORTANT: Remember to register the service in the Manifest and add the BIND_JOB_SERVICE
 * permission.
 *
 * Things to Note:
 * 1) When running on Android Oreo devices or higher, the JobIntentService uses the JobScheduler.
 * This will handle the Wake Locks for you. On any device pre Android Oreo, the JobIntentService
 * will set up Wake Locks through the PowerManager, so make sure you require the WAKE_LOCK
 * permission in your manifest.
 * <uses-permission android:name="android.permission.WAKE_LOCK"/>
 * 2) When running on anything less than Android Oreo the service will start almost instantly.
 * On Android Oreo it will be subject to JobScheduler policies, in other words, it will not run
 * while the device is dozing and may get delayed more than a usual service when the system is
 * under heavy load.
 * 3) On pre Android Oreo the service can run indefinitely, but on Android Oreo it will adhere
 * to the usual JobService execution type limits. At which point it will stop (not the process)
 * and continue execution at a later time.
 * See: https://android.jlelse.eu/keep-those-background-services-working-when-targeting-android-oreo-sdk-26-cbf6cc2bdb7f
 */
class UltravioletWidgetUpdateService : JobIntentService() {

    private val isLocationProviderAvailableUseCase = IsLocationProviderAvailableUseCase(Injection.provideLocationRepository())
    private val getUltravioletIndexUseCase = GetUltravioletIndexUseCase(Injection.provideLocationRepository(), Injection.provideUltravioletIndexRepository())

    public override fun onHandleWork(intent: Intent) {
        // We have received work to do. The system or framework is already holding a wake lock
        // for us at this point, so we can just go.
        Timber.d("Executing work: %s", intent)

        val countDownLatch = CountDownLatch(1)

        // Check whether location and location in background permissions are granted
        if (ContextCompat.checkSelfPermission(this, LOCATION_PERMISSION) != PackageManager.PERMISSION_GRANTED) {
            // Location permission has not been granted, send a notification
            UltravioletWidgetView.showNoLocationPermissionNotification(this)
            // Post code to run on the main UI Thread
            val handler = Handler(Looper.getMainLooper())
            handler.post {
                refreshWidgets(intent, LocationServiceState.Failure.LocationServiceNoPermission)
            }
            countDownLatch.countDown()
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                // In Android 10 and higher background location permission is required for the widget
                if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // Background location permission has not been granted, send a notification
                    UltravioletWidgetView.showNoBackgroundLocationPermissionNotification(this)
                    val handler = Handler(Looper.getMainLooper())
                    handler.post {
                        refreshWidgets(intent, LocationServiceState.Failure.BackgroundLocationNoPermission)
                    }
                    countDownLatch.countDown()
                } else {
                    isLocationProviderAvailableUseCase.execute(IsLocationProviderAvailableObserver(intent, countDownLatch))
                }
            } else {
                // Android 9 and below only need location permission. Get the location provider availability (service
                // is on), and if it is available, chain with get ultraviolet index use case.
                isLocationProviderAvailableUseCase.execute(IsLocationProviderAvailableObserver(intent, countDownLatch))
            }
        }

        Timber.d("Waiting for results...")
        try {
            // Wait until the refresh for all widgets is completed, or the use cases will
            // be disposed after exiting this function without a chance of completing its
            // task.
            countDownLatch.await()
        } catch (e: InterruptedException) {
            Timber.e("Waiting for completion was interrupted: %s", e.message)
        }
    }

    private fun refreshWidgets(intent: Intent, locationServiceState: LocationServiceState) {
        val appWidgetIds = intent.getIntArrayExtra(WIDGET_IDS_KEY)
        if (appWidgetIds != null && appWidgetIds.isNotEmpty()) {
            for (appWidgetId in appWidgetIds) {
                updateWidgetViews(appWidgetId, locationServiceState)
            }
        } else {
            Timber.w("There are no widgets to update")
        }
    }

    private fun refreshWidgets(intent: Intent, ultravioletIndexState: UltravioletIndexState) {
        val appWidgetIds = intent.getIntArrayExtra(WIDGET_IDS_KEY)
        if (appWidgetIds != null && appWidgetIds.isNotEmpty()) {
            for (appWidgetId in appWidgetIds) {
                updateWidgetViews(appWidgetId, ultravioletIndexState)
            }
            if (ultravioletIndexState is Success) {
                removeLocationNotifications()
            }
        } else {
            Timber.e("There are no widgets to update")
        }
    }

    override fun onStopCurrentWork(): Boolean {
        Timber.d("Stopping")

        // Dispose use cases
        isLocationProviderAvailableUseCase.dispose()
        getUltravioletIndexUseCase.dispose()

        return super.onStopCurrentWork()
    }

    override fun onDestroy() {
        Timber.d("Destroying")

        // Dispose use cases
        isLocationProviderAvailableUseCase.dispose()
        getUltravioletIndexUseCase.dispose()
    }

    private fun updateWidgetViews(appWidgetId: Int, locationServiceState: LocationServiceState) {
        UltravioletWidgetView.updateWidgetViews(this, appWidgetId, locationServiceState)
    }

    private fun updateWidgetViews(appWidgetId: Int, ultravioletIndexState: UltravioletIndexState) {
        UltravioletWidgetView.updateWidgetViews(this, appWidgetId, ultravioletIndexState)
    }

    private fun removeLocationNotifications() {
        UltravioletWidgetView.removeLocationNotifications(this)
    }

    private inner class IsLocationProviderAvailableObserver(
        val intent: Intent,
        val countDownLatch: CountDownLatch
    ) : DisposableSingleObserver<Boolean>() {

        override fun onSuccess(available: Boolean) {
            if (!available) {
                UltravioletWidgetView.showLocationServiceNotification(this@UltravioletWidgetUpdateService)
                refreshWidgets(intent, LocationServiceState.Failure.LocationServiceOff)
                countDownLatch.countDown()
            } else {
                // Access to location service is granted and location service is available, too
                getUltravioletIndexUseCase.execute(CurrentUltravioletIndexObserver(intent, countDownLatch))
            }
        }

        override fun onError(e: Throwable) {
            Timber.e("Error: ${e.message}")
            refreshWidgets(intent, LocationServiceState.Failure.Error(e))
            countDownLatch.countDown()
        }
    }

    private inner class CurrentUltravioletIndexObserver(
        val intent: Intent,
        val countDownLatch: CountDownLatch
    ) : DisposableSingleObserver<CurrentUltravioletIndex>() {

        override fun onSuccess(currentUltravioletIndex: CurrentUltravioletIndex) {
            Timber.d("Ultraviolet index was obtained successfully")
            refreshWidgets(intent, Success(currentUltravioletIndex))
            countDownLatch.countDown()
        }

        override fun onError(e: Throwable) {
            Timber.e("An error was found: %s", e.toString())
            refreshWidgets(intent, Failure(e))
            countDownLatch.countDown()
        }
    }

    companion object {
        const val WIDGET_IDS_KEY = "com.xme.client.android.ultravioletindex.app.widget.UltravioletWidgetUpdateService.WIDGET_IDS_KEY"
        private const val JOB_ID = 1000

        /**
         * Convenience method for enqueuing work in this service.
         */
        fun enqueueWork(context: Context, work: Intent) {
            enqueueWork(context, UltravioletWidgetUpdateService::class.java, JOB_ID, work)
        }
    }
}
