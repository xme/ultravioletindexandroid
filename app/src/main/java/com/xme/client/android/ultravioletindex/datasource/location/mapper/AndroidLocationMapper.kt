/*
 * Copyright (C) 2018 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.datasource.location.mapper

import com.xme.client.android.ultravioletindex.datasource.remote.mapper.RemoteModelMapper
import android.location.Location as AndroidLocation
import com.xme.client.android.ultravioletindex.model.location.Location as ModelLocation

/**
 * Map a [android.location.Location] to a [com.xme.client.android.ultravioletindex.model.location.Location]
 * instance when data is moving between this later and the data layer.
 */
class AndroidLocationMapper : RemoteModelMapper<AndroidLocation, ModelLocation> {

    override fun mapFromRemote(type: AndroidLocation): ModelLocation {
        return ModelLocation(
            type.provider,
            type.latitude,
            type.longitude,
            type.altitude,
            type.time
        )
    }
}