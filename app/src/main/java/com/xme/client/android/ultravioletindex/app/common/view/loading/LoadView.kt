/*
 * Copyright (C) 2019 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.app.common.view.loading

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import com.xme.client.android.ultravioletindex.BuildConfig
import com.xme.client.android.ultravioletindex.databinding.ViewLoadingBinding

/**
 * View used to display loading state to the user.
 */
class LoadView : RelativeLayout {

    private val binding = ViewLoadingBinding.inflate(LayoutInflater.from(context), this)

    private var message: String = ""
        set(value) {
            field = value
            // Post a task to update the view
            binding.tvLoadingViewMessage.post {
                // When the task is executed, check that the view is still there
                binding.tvLoadingViewMessage.let {
                    // Update the message
                    binding.tvLoadingViewMessage.text = field
                }
            }
        }

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) :
            super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
    }

    override fun onFinishInflate() {
        super.onFinishInflate()

        if (BuildConfig.DEBUG) {
            binding.tvLoadingViewMessage.visibility = View.VISIBLE
        }
    }

    fun showView(message: String = "") {
        this.visibility = View.VISIBLE
        this.message = message
    }

    fun hideView() {
        this.visibility = View.GONE
        this.message = ""
    }
}