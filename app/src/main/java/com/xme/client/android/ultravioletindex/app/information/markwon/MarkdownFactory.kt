/*
 * Copyright (C) 2019 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.app.information.markwon

import android.content.Context
import com.xme.client.android.ultravioletindex.app.information.markwon.gif.GifAwarePlugin
import io.noties.markwon.Markwon
import io.noties.markwon.ext.strikethrough.StrikethroughPlugin
import io.noties.markwon.ext.tables.TablePlugin
import io.noties.markwon.ext.tasklist.TaskListPlugin
import io.noties.markwon.html.HtmlPlugin
import io.noties.markwon.image.glide.GlideImagesPlugin
import io.noties.markwon.syntax.Prism4jTheme
import io.noties.markwon.syntax.SyntaxHighlightPlugin
import io.noties.prism4j.Prism4j
import io.noties.prism4j.annotations.PrismBundle

@PrismBundle(
    includeAll = true,
    grammarLocatorClassName = ".GrammarLocatorDef"
)
object MarkdownFactory {

    @JvmStatic
    fun build(context: Context, prism4jTheme: Prism4jTheme): Markwon {

        return Markwon.builder(context)
//            .usePlugin(ImagesPlugin.create { plugin ->
//                // data uri scheme handler is added automatically
//                // SVG & GIF will be added if required dependencies are present in the classpath
//                // default-media-decoder is also added automatically
//                plugin
//                    .addSchemeHandler(OkHttpNetworkSchemeHandler.create())
//                    .addSchemeHandler(FileSchemeHandler.createWithAssets(context.assets))
//            })
            //.usePlugin(MovementMethodPlugin.create(ScrollingMovementMethod.getInstance()))
            .usePlugin(GlideImagesPlugin.create(context))
            .usePlugin(SyntaxHighlightPlugin.create(Prism4j(GrammarLocatorDef()), prism4jTheme))
            .usePlugin(GifAwarePlugin.create(context))
            .usePlugin(TablePlugin.create(context))
            .usePlugin(TaskListPlugin.create(context))
            .usePlugin(StrikethroughPlugin.create())
            .usePlugin(HtmlPlugin.create())
//            .usePlugin(object : AbstractMarkwonPlugin() {
//                override fun configureConfiguration(builder: MarkwonConfiguration.Builder) {
//                    builder.urlProcessor(urlProcessor)
//                }
//            })
            .build()
    }
}