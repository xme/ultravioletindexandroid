/*
 * Copyright (C) 2018 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex

import android.app.Application
import android.content.Context
import com.xme.client.android.ultravioletindex.app.notification.NotificationUtils
import com.xme.client.android.ultravioletindex.app.widget.notification.location.LocationPermissionNotification
import com.xme.client.android.ultravioletindex.common.log.TimberLogImplementation

class AndroidApplication : Application() {

    init {
        instance = this
    }

    companion object {
        private lateinit var instance: AndroidApplication

        fun applicationContext(): Context {
            return instance
        }
    }

    override fun onCreate() {
        super.onCreate()

        // Initialize logging library
        TimberLogImplementation.init(applicationContext)

        // Initialize notification channels
        NotificationUtils.createNotificationChannel(this, LocationPermissionNotification.buildNoLocationPermissionNotification(this))
    }
}
