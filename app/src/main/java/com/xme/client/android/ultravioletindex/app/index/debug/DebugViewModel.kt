/*
 * Copyright (C) 2019 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.app.index.debug

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.xme.client.android.ultravioletindex.app.common.state.LocationServiceState
import com.xme.client.android.ultravioletindex.app.index.debug.state.LocationState
import com.xme.client.android.ultravioletindex.app.index.debug.state.UltravioletIndexForLocationState
import com.xme.client.android.ultravioletindex.domain.location.interactor.GetLastLocationUseCase
import com.xme.client.android.ultravioletindex.domain.location.interactor.IsLocationProviderAvailableUseCase
import com.xme.client.android.ultravioletindex.domain.ultravioletindex.interactor.GetUltravioletIndexForLocationUseCase
import com.xme.client.android.ultravioletindex.injection.Injection
import com.xme.client.android.ultravioletindex.model.location.Location
import com.xme.client.android.ultravioletindex.model.ultravioletindex.CurrentUltravioletIndex
import io.reactivex.rxjava3.observers.DisposableMaybeObserver
import io.reactivex.rxjava3.observers.DisposableSingleObserver
import timber.log.Timber

class DebugViewModel : ViewModel() {

    // https://gps-coordinates.org
    // https://www.timeanddate.com/astronomy
    // http://sunburnmap.com
    private val zaragoza = doubleArrayOf(41.6488226, -0.8890853000000334)
    private val newYork = doubleArrayOf(40.7127753, -74.0059728)
    private val brasilia = doubleArrayOf(-15.7942287, -47.882165799999996)
    private val tokyo = doubleArrayOf(35.6894875, 139.69170639999993)
    private val canberra = doubleArrayOf(-35.2809368, 149.13000920000002)

    private val latitude = zaragoza[0]
    private val longitude = zaragoza[1]
    private val isLocationProviderAvailableUseCase =
        IsLocationProviderAvailableUseCase(Injection.provideLocationRepository())
    private val getLastLocationUseCase =
        GetLastLocationUseCase(Injection.provideLocationRepository())
    private val getUltravioletIndexUseCase =
        GetUltravioletIndexForLocationUseCase(Injection.provideUltravioletIndexRepository())

    val isLocationProviderAvailableSource: LiveData<LocationServiceState>
        get() = isLocationProviderAvailableMutableSource

    val lastLocationSource: LiveData<LocationState>
        get() = lastLocationMutableSource

    val ultravioletIndexSource: LiveData<UltravioletIndexForLocationState>
        get() = ultravioletIndexMutableSource

    private val isLocationProviderAvailableMutableSource = MutableLiveData<LocationServiceState>()
    private val lastLocationMutableSource = MutableLiveData<LocationState>()
    private val ultravioletIndexMutableSource = MutableLiveData<UltravioletIndexForLocationState>()

    private inner class LocationProviderAvailabilityObserver : DisposableSingleObserver<Boolean>() {

        override fun onSuccess(available: Boolean) {
            Timber.d("Location provider availability: $available")

            if (!available) {
                isLocationProviderAvailableMutableSource.value = LocationServiceState.Failure.LocationServiceOff
            } else {
                // Access to location service is granted and location service is available, too
                isLocationProviderAvailableMutableSource.value = LocationServiceState.Available
            }
        }

        override fun onError(e: Throwable) {
            Timber.e("Error: ${e.message}")
            isLocationProviderAvailableMutableSource.value = LocationServiceState.Failure.Error(e)
        }
    }

    private inner class LastLocationObserver : DisposableMaybeObserver<Location>() {

        override fun onSuccess(location: Location) {
            Timber.d("Location: $location")
            lastLocationMutableSource.value = LocationState.Success(location)
        }

        override fun onComplete() {
            Timber.w("There is no location available")
            lastLocationMutableSource.value = LocationState.Failure.NoLocationAvailable
        }

        override fun onError(e: Throwable) {
            Timber.e("Error: ${e.message}")
            lastLocationMutableSource.value = LocationState.Failure.Error(e)
        }
    }

    private inner class UltravioletIndexObserver : DisposableSingleObserver<CurrentUltravioletIndex>() {

        override fun onSuccess(ultravioletIndex: CurrentUltravioletIndex) {
            Timber.d("UV index: ${ultravioletIndex.value}")
            ultravioletIndexMutableSource.value = UltravioletIndexForLocationState.Success(ultravioletIndex)
        }

        override fun onError(e: Throwable) {
            Timber.e("Error: ${e.message}")
            ultravioletIndexMutableSource.value = UltravioletIndexForLocationState.Failure(e)
        }
    }

    fun getUltravioletIndex() {
        // setValue() vs postValue()
        // * setValue() is called directly from caller thread and synchronously notifies observers.
        // Hence, this method must be called from the main thread.
        // * postValue() posts a task to a main thread to set the given value. Use it when you need
        // set a value from a background thread. If you called this method multiple times before a
        // main thread executed a posted task, only the last value would be dispatched.
        //
        // See: https://stackoverflow.com/questions/51299641/difference-of-setvalue-postvalue-in-mutablelivedata
        ultravioletIndexMutableSource.value = UltravioletIndexForLocationState.Loading
        val params = GetUltravioletIndexForLocationUseCase.Params.withLocation(latitude, longitude)
        getUltravioletIndexUseCase.execute(UltravioletIndexObserver(), params)
    }

    fun isLocationProviderAvailable() {
        isLocationProviderAvailableMutableSource.value = LocationServiceState.Loading
        isLocationProviderAvailableUseCase.execute(LocationProviderAvailabilityObserver())
    }

    fun getLastLocation() {
        lastLocationMutableSource.value = LocationState.Loading
        getLastLocationUseCase.execute(LastLocationObserver())
    }

    override fun onCleared() {
        super.onCleared()

        isLocationProviderAvailableUseCase.dispose()
        getLastLocationUseCase.dispose()
        getUltravioletIndexUseCase.dispose()
    }
}
