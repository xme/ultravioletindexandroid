/*
 * Copyright (C) 2019 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.datasource.remote.errorhandling

import com.xme.client.android.ultravioletindex.model.exception.HTTPException
import retrofit2.HttpException
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import timber.log.Timber
import java.io.IOException

/**
 * Maps Retrofit [HttpException] to custom defined [HTTPException].
 *
 * Retrofit [RxJava3CallAdapterFactory] returns within the observable error channel an [HttpException], which
 * is defined in Retrofit. This means that the exception can be propagated to other layers than data source,
 * such as data, domain, or even app. In order to deal with Retrofit exceptions in other layers, these layers
 * must have Retrofit as a dependency, which will make your whole app coupled to this library, instead of just
 * the remote package of data source layer. To cope with this, Retrofit [HttpException] can be mapped to a
 * similar app defined [HTTPException]. This app defined exception can be placed together with models, so that
 * when a result is received, it is either an app model or exception, but nothing from an external library.
 */
object RetrofitRxAdapterExceptionMapper {

    fun getException(throwable: Throwable): Throwable {

        return when (throwable) {
            // Map Retrofit HTTP exception
            is HttpException -> {
                val statusCode = throwable.code()
                val responseMessage = throwable.message()
                val responseBody = throwable.response()?.errorBody()
                try {
                    HTTPException(responseMessage, throwable, statusCode, responseBody?.string())
                } catch (e: IOException) {
                    Timber.e(e, "Retrofit HTTP exception body cannot be extracted")
                    IllegalArgumentException("Retrofit HTTP exception cannot be parsed")
                }
            }
            // Return other throwable as-is
            else -> throwable
        }
    }
}
