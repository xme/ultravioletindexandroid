/*
 * Copyright (C) 2019 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.app.index.debug

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import com.google.android.material.snackbar.Snackbar
import com.xme.client.android.ultravioletindex.R
import com.xme.client.android.ultravioletindex.app.common.LOCATION_PERMISSION
import com.xme.client.android.ultravioletindex.app.common.error.ErrorBundleBuilder
import com.xme.client.android.ultravioletindex.app.common.error.RetryAction
import com.xme.client.android.ultravioletindex.app.common.error.RetryAction.GetLastLocation
import com.xme.client.android.ultravioletindex.app.common.error.RetryAction.GetUltravioletIndex
import com.xme.client.android.ultravioletindex.app.common.error.RetryAction.RefreshWidget
import com.xme.client.android.ultravioletindex.app.common.event.Event
import com.xme.client.android.ultravioletindex.app.common.exception.LocationServiceNoPermissionException
import com.xme.client.android.ultravioletindex.app.common.state.LocationServiceState
import com.xme.client.android.ultravioletindex.app.common.state.LocationServiceState.Failure.BackgroundLocationNoPermission
import com.xme.client.android.ultravioletindex.app.common.view.BaseFragment
import com.xme.client.android.ultravioletindex.app.common.view.error.ErrorListener
import com.xme.client.android.ultravioletindex.app.dialog.GenericDialogFragmentViewModel
import com.xme.client.android.ultravioletindex.app.dialog.GenericDialogResult
import com.xme.client.android.ultravioletindex.app.dialog.GenericDialogResult.Cancelled
import com.xme.client.android.ultravioletindex.app.dialog.GenericDialogResult.Negative
import com.xme.client.android.ultravioletindex.app.dialog.GenericDialogResult.Positive
import com.xme.client.android.ultravioletindex.app.index.debug.state.LocationState
import com.xme.client.android.ultravioletindex.app.index.debug.state.UltravioletIndexForLocationState
import com.xme.client.android.ultravioletindex.app.index.dialog.DialogAction
import com.xme.client.android.ultravioletindex.app.index.dialog.DialogAction.OPEN_GOOGLE_MAPS
import com.xme.client.android.ultravioletindex.app.index.dialog.DialogAction.OPEN_LOCATION_SETTINGS
import com.xme.client.android.ultravioletindex.app.index.dialog.DialogAction.SHOW_BACKGROUND_LOCATION_SERVICE_RATIONALE
import com.xme.client.android.ultravioletindex.app.index.dialog.DialogAction.SHOW_LOCATION_SERVICE_RATIONALE
import com.xme.client.android.ultravioletindex.app.index.error.IndexErrorBundleBuilder
import com.xme.client.android.ultravioletindex.app.widget.util.WidgetUtils
import com.xme.client.android.ultravioletindex.data.util.JsonUtils
import com.xme.client.android.ultravioletindex.databinding.DebugFragmentBinding
import com.xme.client.android.ultravioletindex.model.location.exception.NoLocationAvailableException
import timber.log.Timber

class DebugFragment : BaseFragment() {

    private var _binding: DebugFragmentBinding? = null
    private val binding get() = _binding!!

    private val genericDialogFragmentViewModel: GenericDialogFragmentViewModel by activityViewModels()
    private val viewModel: DebugViewModel by navGraphViewModels(R.id.index_nav_graph) // This view model is only used in this graph
    private lateinit var errorBundleBuilder: ErrorBundleBuilder

    companion object {

        private const val REQUEST_LOCATION_PERMISSION = 0
        private const val REQUEST_BACKGROUND_LOCATION_PERMISSION = 1
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = DebugFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun initializeViews(savedInstanceState: Bundle?) {
        super.initializeViews(savedInstanceState)

        initializeErrorView()

        binding.btDebugLastKnownLocationButton.setOnClickListener {
            getLastLocation()
        }
        binding.btDebugGetUltravioletIndexButton.setOnClickListener {
            viewModel.getUltravioletIndex()
        }
        binding.btDebugRefreshWidgetButton.setOnClickListener {
            refreshWidgets()
        }
        binding.btDebugCrashAppButton.setOnClickListener {
            throw RuntimeException("Crashlytics test crash")
        }
    }

    override fun initializeContents(savedInstanceState: Bundle?) {
        super.initializeContents(savedInstanceState)

        // Listen to view model events published by the dialogs shown in this fragment
        genericDialogFragmentViewModel.genericDialogResultEvent.observe(viewLifecycleOwner, Observer<Event<GenericDialogResult>> { event ->
            event.getContentIfNotHandled()?.let { result ->
                // Only proceed if the event has never been handled
                when (result) {
                    is Positive -> onGenericDialogPositiveAction(result.actionId)
                    is Negative -> onGenericDialogNegativeAction(result.actionId)
                    is Cancelled -> onGenericDialogCancelled(result.actionId)
                }
            }
        })

        // Link the fragment and the view model with "viewLifecycleOwner", so that observers
        // can be subscribed in onActivityCreated() and can be automatically unsubscribed
        // in onDestroyView().
        // IMPORTANT: Never use "this" as lifecycle owner.
        // See: https://medium.com/@BladeCoder/architecture-components-pitfalls-part-1-9300dd969808
        viewModel.isLocationProviderAvailableSource.observe(viewLifecycleOwner, Observer<LocationServiceState> {
            when (it) {
                LocationServiceState.Loading -> displayLoadingState(getString(R.string.index_is_location_service_available_loading))
                LocationServiceState.Available -> {
                    // Permission is granted and location service is running, get last known location
                    viewModel.getLastLocation()
                }
                LocationServiceState.Failure.LocationServiceOff -> {
                    binding.lvDebugLoadingView.hideView()
                    // Location service is off
                    askUser(
                        android.R.drawable.ic_dialog_alert,
                        R.string.location_service_off_title,
                        R.string.location_service_off_message,
                        OPEN_LOCATION_SETTINGS.name
                    )
                }
                // LocationServiceNoPermission should never reached, but it is also implemented, just in case :)
                LocationServiceState.Failure.LocationServiceNoPermission -> displayFailureState(LocationServiceNoPermissionException(), GetLastLocation)
                BackgroundLocationNoPermission -> throw IllegalStateException("No background location permission should never be received.")
                is LocationServiceState.Failure.Error -> displayFailureState(it.throwable, GetLastLocation)
            }
        })

        viewModel.lastLocationSource.observe(viewLifecycleOwner, Observer<LocationState> {
            when (it) {
                LocationState.Loading -> displayLoadingState(getString(R.string.index_get_last_known_location_loading))
                is LocationState.Success -> displaySuccessState(JsonUtils.fromPojoToJson(it.location))
                is LocationState.Failure.NoLocationAvailable -> displayFailureState(NoLocationAvailableException(), GetLastLocation)
                is LocationState.Failure.Error -> displayFailureState(it.throwable, GetLastLocation)
            }
        })

        viewModel.ultravioletIndexSource.observe(viewLifecycleOwner, Observer<UltravioletIndexForLocationState> {
            when (it) {
                UltravioletIndexForLocationState.Loading -> displayLoadingState(getString(R.string.index_get_ultraviolet_index_loading))
                is UltravioletIndexForLocationState.Success -> displaySuccessState(JsonUtils.fromPojoToJson(it.currentUltravioletIndex))
                is UltravioletIndexForLocationState.Failure -> displayFailureState(it.throwable, GetUltravioletIndex)
            }
        })
    }

    private fun refreshWidgets() {
        context?.let {
            WidgetUtils.refreshWidgets(it)
        } ?: Timber.e("Context is null")
    }

    private fun getLastLocation() {
        context?.let {
            // Check if the location permission has been granted
            if (ContextCompat.checkSelfPermission(it, LOCATION_PERMISSION) == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                Timber.v("Location permission is granted")
                viewModel.isLocationProviderAvailable()
            } else {
                // Permission has not been granted and must be requested
                if (shouldShowRequestPermissionRationale(LOCATION_PERMISSION)) {
                    Timber.v("Rationale should be shown")
                    // Provide an additional rationale to the user if the permission was not granted
                    // and the user would benefit from additional context for the use of the permission.
                    askUser(
                        0,
                        R.string.location_permission_rationale_title,
                        R.string.location_permission_rationale_message,
                        SHOW_LOCATION_SERVICE_RATIONALE.name
                    )
                } else {
                    Timber.v("Permission is not available. Requesting permission")

                    // Request the permission. The result will be received in onRequestPermissionResult().
                    requestPermissions(arrayOf(LOCATION_PERMISSION), REQUEST_LOCATION_PERMISSION)
                }
            }
        } ?: Timber.e("Context is null")
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_LOCATION_PERMISSION -> {
                // If request is cancelled, results array is empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Timber.v("Location permission has been granted")

                    // Permission has been granted
                    Snackbar.make(binding.clDebugLayout, "Location permission has been granted.", Snackbar.LENGTH_SHORT).show()
                    viewModel.getLastLocation()
                } else {
                    Timber.v("Location permission was denied")

                    // Permission request was denied.
                    Snackbar.make(binding.clDebugLayout, "Location permission was denied.", Snackbar.LENGTH_SHORT).show()
                }
            }
            // other 'case' lines to check for other
            // permissions this app might request.
            else -> Timber.w("A permission result with an unknown request code was received")
        }
    }

    private fun askUser(@DrawableRes icon: Int, @StringRes title: Int, @StringRes message: Int, actionName: String) {
        val action = DebugFragmentDirections.actionDebugFragmentToGenericDialogFragment(
            icon,
            title,
            message,
            false,
            R.string.yes,
            R.string.no,
            actionName
        )
        findNavController().navigate(action)
    }

    private fun onGenericDialogPositiveAction(action: String) {
        when (DialogAction.valueOf(action)) {
            OPEN_LOCATION_SETTINGS -> {
                context?.let { context ->
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    if (intent.resolveActivity(context.packageManager) != null) {
                        startActivity(intent)
                    }
                } ?: Timber.e("Context is null")
            }
            SHOW_LOCATION_SERVICE_RATIONALE -> {
                // Request the permission
                requestPermissions(arrayOf(LOCATION_PERMISSION), REQUEST_LOCATION_PERMISSION)
            }
            SHOW_BACKGROUND_LOCATION_SERVICE_RATIONALE -> {
                // Request the background permission
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                    requestPermissions(arrayOf(android.Manifest.permission.ACCESS_BACKGROUND_LOCATION), REQUEST_BACKGROUND_LOCATION_PERMISSION)
                }
            }
            OPEN_GOOGLE_MAPS -> {
                val uri = "http://maps.google.com/maps"
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                intent.setPackage("com.google.android.apps.maps")
                startActivity(intent)
            }
        }
    }

    private fun onGenericDialogNegativeAction(action: String) {
        when (DialogAction.valueOf(action)) {
            SHOW_LOCATION_SERVICE_RATIONALE -> {
                Timber.d("User does not want to grant location permission to the app")
            }
            SHOW_BACKGROUND_LOCATION_SERVICE_RATIONALE -> {
                Timber.d("User does not want to grant background location permission to the app")
            }
            OPEN_LOCATION_SETTINGS -> {
                Timber.d("User does not want to open location settings screen")
            }
            OPEN_GOOGLE_MAPS -> {
                Timber.d("User does not want to open Google Maps for caching a location")
            }
        }
    }

    private fun onGenericDialogCancelled(action: String) {
        when (DialogAction.valueOf(action)) {
            SHOW_LOCATION_SERVICE_RATIONALE -> {
                Timber.d("Location rationale dialog was cancelled by user")
            }
            SHOW_BACKGROUND_LOCATION_SERVICE_RATIONALE -> {
                Timber.d("Background location rationale dialog was cancelled by user")
            }
            OPEN_LOCATION_SETTINGS -> {
                Timber.d("Open location dialog was cancelled by user")
            }
            OPEN_GOOGLE_MAPS -> {
                Timber.d("Open Google Maps dialog was cancelled by user")
            }
        }
    }

    private fun displayLoadingState(message: String) {
        binding.lvDebugLoadingView.showView(message)
        binding.clDebugLayout.visibility = View.VISIBLE
        binding.evDebugErrorView.visibility = View.GONE
    }

    private fun displaySuccessState(data: String) {
        binding.tvDebugResultText.text = data

        binding.lvDebugLoadingView.hideView()
        binding.clDebugLayout.visibility = View.VISIBLE
        binding.evDebugErrorView.visibility = View.GONE
    }

    private fun displayFailureState(throwable: Throwable, retryAction: RetryAction) {
        binding.evDebugErrorView.errorBundle = errorBundleBuilder.build(throwable, retryAction)

        binding.lvDebugLoadingView.hideView()
        binding.clDebugLayout.visibility = View.GONE
        binding.evDebugErrorView.visibility = View.VISIBLE

        if (throwable is NoLocationAvailableException) {
            askUser(
                0,
                R.string.location_not_available_title,
                R.string.location_not_available_message,
                OPEN_GOOGLE_MAPS.name
            )
        }
    }

    private fun initializeErrorView() {
        errorBundleBuilder = IndexErrorBundleBuilder()
        binding.evDebugErrorView.errorListener = errorListener
    }

    private val errorListener = object : ErrorListener {
        override fun onRetry(retryAction: RetryAction) {
            when (retryAction) {
                GetLastLocation -> getLastLocation()
                GetUltravioletIndex -> viewModel.getUltravioletIndex()
                RefreshWidget -> refreshWidgets()
            }
        }
    }
}
