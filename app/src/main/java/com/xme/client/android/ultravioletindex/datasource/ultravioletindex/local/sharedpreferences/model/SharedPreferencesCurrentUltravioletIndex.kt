/*
 * Copyright (C) 2018 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.datasource.ultravioletindex.local.sharedpreferences.model

import androidx.annotation.FloatRange
import androidx.annotation.IntRange
import com.google.gson.annotations.SerializedName

data class SharedPreferencesCurrentUltravioletIndex(
    @SerializedName(value = "latitude", alternate = ["lat"])
    @FloatRange(from = -90.0, to = 90.0)
    val latitude: Double,
    @SerializedName(value = "longitude", alternate = ["lon"])
    @FloatRange(from = -180.0, to = 180.0)
    val longitude: Double,
    @SerializedName("date")
    @IntRange(from = 0)
    val date: Long,
    @SerializedName("value")
    @FloatRange(from = 0.0)
    val value: Double,
    @SerializedName("sunrise")
    @IntRange(from = 0)
    val sunrise: Long,
    @SerializedName("sunset")
    @IntRange(from = 0)
    val sunset: Long,
    @SerializedName("timeZoneId")
    val timeZoneId: String
)