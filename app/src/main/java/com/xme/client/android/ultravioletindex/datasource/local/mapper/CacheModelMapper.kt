/*
 * Copyright (C) 2018 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.datasource.local.mapper

/**
 * Interface for data source cache model mappers used for transforming models into cache models and vice versa.
 *
 * @param <C> the cached model
 * @param <M> the model
 */
interface CacheModelMapper<C, M> {

    fun mapFromCached(type: C): M

    fun mapToCached(type: M): C
}