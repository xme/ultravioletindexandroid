/*
 * Copyright (C) 2018 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.app.notification

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build

/**
 * Simplifies common [Notification] tasks.
 */
object NotificationUtils {

    fun createNotificationChannel(
        context: Context,
        channelNotificationData: ChannelNotificationData
    ): String? {

        // NotificationChannels are required for notifications on O (API 26) and above.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            // The id of the channel.
            val channelId = channelNotificationData.channelId

            // The user-visible name of the channel.
            val channelName = channelNotificationData.channelName
            // The user-visible description of the channel.
            val channelDescription = channelNotificationData.channelDescription
            val channelImportance = channelNotificationData.channelImportance
            val channelEnableVibrate = channelNotificationData.isChannelEnableVibrate
            val channelEnableLight = channelNotificationData.isChannelEnableLight
            val channelLightColor = channelNotificationData.channelLightColor
            val channelLockscreenVisibility = channelNotificationData.channelLockScreenVisibility

            // Initializes NotificationChannel.
            val notificationChannel = NotificationChannel(channelId, channelName, channelImportance)
            notificationChannel.description = channelDescription
            notificationChannel.enableVibration(channelEnableVibrate)
            notificationChannel.enableLights(channelEnableLight)
            notificationChannel.lightColor = channelLightColor
            notificationChannel.lockscreenVisibility = channelLockscreenVisibility

            // Adds NotificationChannel to system. Attempting to create an existing notification
            // channel with its original values performs no operation, so it's safe to perform the
            // below sequence.
            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(notificationChannel)

            return channelId
        } else {
            // Returns null for pre-O (26) devices.
            return null
        }
    }
}
