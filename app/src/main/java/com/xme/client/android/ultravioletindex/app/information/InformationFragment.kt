/*
 * Copyright (C) 2019 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.app.information

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.xme.client.android.ultravioletindex.app.common.view.BaseFragment
import com.xme.client.android.ultravioletindex.app.information.markwon.MarkdownFactory
import com.xme.client.android.ultravioletindex.app.information.markwon.MarkdownHelper
import com.xme.client.android.ultravioletindex.databinding.InformationFragmentBinding
import io.noties.markwon.syntax.Prism4jThemeDefault
import io.noties.markwon.utils.NoCopySpannableFactory
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import timber.log.Timber

class InformationFragment : BaseFragment() {

    private var _binding: InformationFragmentBinding? = null
    private val binding get() = _binding!!

    private val disposables = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = InformationFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun initializeViews(savedInstanceState: Bundle?) {
        super.initializeViews(savedInstanceState)

        binding.tvInformationText.movementMethod = LinkMovementMethod.getInstance()
        binding.tvInformationText.setSpannableFactory(NoCopySpannableFactory.getInstance())
    }

    override fun initializeContents(savedInstanceState: Bundle?) {
        super.initializeContents(savedInstanceState)

        context?.let { context ->
            val markwon = MarkdownFactory.build(context, Prism4jThemeDefault.create())
            disposables.add(
                MarkdownHelper.load(context)
                .flatMap { mardownText ->
                    MarkdownHelper.render(markwon, mardownText)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    markwon.setParsedMarkdown(binding.tvInformationText, it)
                }, {
                    Timber.e("Mardown cannot be displayed: ${it.message}")
                }))
        } ?: Timber.e("Context is null!!!")
    }
}
