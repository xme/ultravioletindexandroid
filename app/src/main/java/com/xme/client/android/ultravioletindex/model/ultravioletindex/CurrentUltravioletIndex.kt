/*
 * Copyright (C) 2019 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.model.ultravioletindex

import androidx.annotation.FloatRange
import androidx.annotation.IntRange
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.Arrays
import kotlin.math.round
import kotlin.math.sin

data class CurrentUltravioletIndex(
    @SerializedName(value = "latitude", alternate = ["lat"])
    @FloatRange(from = -90.0, to = 90.0)
    val latitude: Double,
    @SerializedName(value = "longitude", alternate = ["lon"])
    @FloatRange(from = -180.0, to = 180.0)
    val longitude: Double,
    @SerializedName("date")
    @IntRange(from = 0)
    val date: Long,
    @SerializedName("value")
    @FloatRange(from = 0.0)
    val value: Double,
    @SerializedName("sunrise")
    @IntRange(from = 0)
    val sunrise: Long,
    @SerializedName("sunset")
    @IntRange(from = 0)
    val sunset: Long,
    @SerializedName("timeZoneId")
    val timeZoneId: String = ZoneId.systemDefault().id
) {

    // Java 8 time API classes are immutable, so there is no need to return a copy of the class member variable. See: https://stackoverflow.com/a/51842627/5189200
    @Expose
    @SerializedName("valueInstant")
    val valueInstant: Instant = Instant.ofEpochSecond(date)

    @Expose
    @SerializedName("sunriseInstant")
    val sunriseInstant: Instant = Instant.ofEpochSecond(sunrise)

    @Expose
    @SerializedName("sunsetInstant")
    val sunsetInstant: Instant = Instant.ofEpochSecond(sunset)

    @Expose
    @SerializedName("uvi")
    val uvi: DoubleArray = initializeUvi()

    val currentUltravioletIndex: Double
        get() {
            val zoneId = ZoneId.of(timeZoneId)
            val currentHour = ZonedDateTime.ofInstant(Instant.now(), zoneId).hour
            return uvi[currentHour]
        }

    val valueZonedDateTime: ZonedDateTime
        get() {
            return ZonedDateTime.ofInstant(valueInstant, ZoneId.of(timeZoneId))
        }

    val sunriseZonedDateTime: ZonedDateTime
        get() {
            return ZonedDateTime.ofInstant(sunriseInstant, ZoneId.of(timeZoneId))
        }

    val sunsetZonedDateTime: ZonedDateTime
        get() {
            return ZonedDateTime.ofInstant(sunsetInstant, ZoneId.of(timeZoneId))
        }

    private fun initializeUvi(): DoubleArray {
        val result = DoubleArray(24)

        if (sunrise > 0 && sunset > 0) {
            // All hours start with UV index equal to zero
            Arrays.fill(result, 0.0)

            // IMPORTANT: This computation only works if the latitude and longitude provided match
            // the local time of the device. In other words, if latitude and longitude are set for
            // New York, but local time is in Beijing, computation will be mess.
            // In the future, if the user is allowed to set an arbitrary location, its corresponding
            // time zone must be retrieved, too.
            // For the time being, time zone is mobile device default one.

            // Get sunrise and sunset date and hour
            // A time zone date time instead of an offset date time may be a better choice
            // See: https://stackoverflow.com/a/41430483/5189200
            //      https://stackoverflow.com/a/43012329
            val sunriseLocalDay = sunriseZonedDateTime.toLocalDate()
            val sunsetLocalDay = sunsetZonedDateTime.toLocalDate()
            val sunriseHour = sunriseZonedDateTime.hour
            val sunsetHour = sunsetZonedDateTime.hour

            // Check sunrise and sunset hours
            if (sunriseLocalDay.isEqual(sunsetLocalDay) &&
                sunriseHour >= 0 && sunsetHour <= 23 &&
                sunriseHour <= sunsetHour) {
                // Compute UV index curve
                // For computing the hours between sunrise and sunset, +1 is needed (from 7 to 19 there are 13 hours)
                val diff = sunsetHour - sunriseHour + 1
                var t = 0.5 // Compute UV index in the middle of the hour
                var i = sunriseHour
                while (i <= sunsetHour) {
                    // A sinus is used to approximate UV index change between sunrise and sunset.
                    // See: https://en.wikipedia.org/wiki/Sine_wave

                    // Only the interval [0...PI] is used, so map current "t" to a point inside [0...PI]
                    val a = t / diff * Math.PI
                    // y = Amplitude * sin( x )
                    result[i] = this.value * sin(a)
                    ++i
                    t += 1.0
                }
            }
        } else {
            // Without sunrise and sunset during all hours the maximum UV index will be shown
            Arrays.fill(result, this.value)
        }

        return result
    }

    fun isUviComputable(): Boolean {
        var result = false

        if (sunrise > 0 && sunset > 0) {
            val sunriseLocalDay = sunriseZonedDateTime.toLocalDate()
            val sunsetLocalDay = sunsetZonedDateTime.toLocalDate()
            val sunriseHour = sunriseZonedDateTime.hour
            val sunsetHour = sunsetZonedDateTime.hour
            if (sunriseLocalDay.isEqual(sunsetLocalDay) &&
                sunriseHour >= 0 && sunsetHour <= 23 &&
                sunriseHour <= sunsetHour) {
                result = true
            }
        }

        return result
    }

    override fun toString(): String {
        return "CurrentUltravioletIndex(latitude=$latitude, longitude=$longitude, date=$date, value=$value, sunrise=$sunrise, sunset=$sunset, timeZoneId='$timeZoneId', valueInstant=$valueInstant, sunriseInstant=$sunriseInstant, sunsetInstant=$sunsetInstant, uvi=${uvi.contentToString()})"
    }

    companion object {
        @JvmStatic
        @IntRange(from = 0)
        fun fromValueToIndex(@FloatRange(from = 0.0) value: Double): Int? {
            val result: Int?

            when {
                value < 0.0 -> result = null
                value == 0.0 -> result = 0
                else -> {
                    // 0.0 < value
                    val roundedValue = round(value).toInt()
                    result =  when (roundedValue) {
                        0 -> 1
                        else -> roundedValue
                    }
                }
            }

            return result
        }
    }
}