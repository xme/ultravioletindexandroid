/*
 * Copyright (C) 2018 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.data.location.source

import com.xme.client.android.ultravioletindex.model.location.Location
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single

/**
 * Interface defining methods for the data operations related to location.
 * This is to be implemented by cache and remote data stores, setting the requirements for the
 * operations that need to be implemented.
 */
interface LocationDataStore {

    fun isLocationProviderAvailable(): Single<Boolean>

    fun getLastLocation(): Maybe<Location>

    fun startLocationUpdates(): Observable<Location>

    fun stopLocationUpdates()
}