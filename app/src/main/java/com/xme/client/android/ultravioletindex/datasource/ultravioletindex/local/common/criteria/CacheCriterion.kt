/*
 * Copyright (C) 2018 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.datasource.ultravioletindex.local.common.criteria

import io.reactivex.rxjava3.core.Single

/**
 * Interface for criteria applied to cached data for deciding whether value is valid.
 *
 * @param <C> the cached model
 */
interface CacheCriterion<C> {

    // TODO: Is there a way of making this interface generic for any cached model, so that latitude and longitude do not appear in function signature?
    fun isValidCache(data: C, latitude: Double, longitude: Double): Single<Boolean>
}