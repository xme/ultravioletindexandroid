/*
 * Copyright (C) 2018 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.app.dialog

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.navArgs
import timber.log.Timber

// A dialog must be a public class with an empty public constructor to properly support and handle
// rotations.
// The new instance function is used to set the parameters with a bundle that will be used later
// to build the dialog in onCreate(). It must be done in this way because Android requires a public
// and empty constructor to properly handling rotation and other events with fragments.
// See:
// http://stackoverflow.com/questions/15459209/passing-argument-to-dialogfragment
// http://stackoverflow.com/questions/17622622/how-to-pass-data-from-a-fragment-to-a-dialogfragment
// http://android.codeandmagic.org/why-android-dialogfragment-confuses-me-part1
// http://android.codeandmagic.org/android-dialogfragment-confuses-part-2
class GenericDialogFragment : DialogFragment() {

    private val safeArgs: GenericDialogFragmentArgs by navArgs()

    // TODO: This is wrong. The view model should be shared only with the fragment that shows the DialogFragment.
    private val viewModel: GenericDialogFragmentViewModel by activityViewModels()
    //private val viewModel: GenericDialogFragmentViewModel by viewModels(ownerProducer = { requireParentFragment() })
    private var positive = false
    private var negative = false
    private var actionName = ""

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return context?.let { context ->
            actionName = safeArgs.actionName

            // Use the Builder class for convenient dialog construction
            val builder = AlertDialog.Builder(context)

            builder.setTitle(resources.getString(safeArgs.titleStringId))
                .setMessage(resources.getString(safeArgs.messageStringId))
                .setPositiveButton(
                    resources.getString(safeArgs.positiveButtonTextStringId)
                ) { _ /* dialog */, _ /* which */ ->
                    positive = true
                    sendBackResult()
                }
            // Negative button is optional
            if (safeArgs.negativeButtonTextStringId > 0) {
                builder.setNegativeButton(
                    resources.getString(safeArgs.negativeButtonTextStringId)
                ) { _ /* dialog */, _ /* which */ ->
                    negative = true
                    sendBackResult()
                }
            }

            // Do NOT use set cancelable with the builder, since it will not work. Control how and when
            // to show the dialog must be done through DialogFragment and not through Dialog.
            // See:
            // http://stackoverflow.com/questions/8906269/alertdialogs-setcancelablefalse-method-not-working
            // http://stackoverflow.com/questions/16480114/dialogfragment-setcancelable-property-not-working
            this.isCancelable = safeArgs.cancelable

            // Icon is optional
            if (safeArgs.icon > 0) {
                builder.setIcon(safeArgs.icon)
            }

            // Create the AlertDialog object and return it
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)

        if (!positive && !negative) {
            sendBackResult()
        }
    }

    // Call this method to send the data back to the parent fragment
    private fun sendBackResult() {
        when {
            positive -> {
                viewModel.publish(GenericDialogResult.Positive(actionName))
            }
            negative -> {
                viewModel.publish(GenericDialogResult.Negative(actionName))
                dialog?.let {
                    it.cancel()
                } ?: Timber.w("Dialog fragment cancel was not called")
            }
            else -> {
                // The user has dismissed the dialog with back button or clicking outside the dialog
                viewModel.publish(GenericDialogResult.Cancelled(actionName))
            }
        }
    }
}
