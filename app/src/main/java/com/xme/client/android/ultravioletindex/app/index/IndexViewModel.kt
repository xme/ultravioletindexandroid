/*
 * Copyright (C) 2019 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.app.index

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.xme.client.android.ultravioletindex.app.common.event.Event
import com.xme.client.android.ultravioletindex.app.common.state.LocationServiceState
import com.xme.client.android.ultravioletindex.app.common.state.UltravioletIndexState
import com.xme.client.android.ultravioletindex.domain.location.interactor.IsLocationProviderAvailableUseCase
import com.xme.client.android.ultravioletindex.domain.ultravioletindex.interactor.GetUltravioletIndexUseCase
import com.xme.client.android.ultravioletindex.injection.Injection
import com.xme.client.android.ultravioletindex.model.ultravioletindex.CurrentUltravioletIndex
import io.reactivex.rxjava3.observers.DisposableSingleObserver
import timber.log.Timber

class IndexViewModel : ViewModel() {

    //region Use cases
    private val isLocationProviderAvailableUseCase =
        IsLocationProviderAvailableUseCase(Injection.provideLocationRepository())
    private val getUltravioletIndexUseCase =
        GetUltravioletIndexUseCase(Injection.provideLocationRepository(), Injection.provideUltravioletIndexRepository())
    //endregion

    //region Public data channels
    /**
     * This property is an event because the observer only wants to see whether location service is available once after
     * each [isLocationProviderAvailable] call, so that when the screen is rotated and the observer (fragment) listens
     * to this channel again, the event is not delivered another time.
     */
    val isLocationProviderAvailableEvent: LiveData<Event<LocationServiceState>>
        get() = isLocationProviderAvailableMutableEvent

    val ultravioletIndexSource: LiveData<UltravioletIndexState>
        get() = ultravioletIndexMutableSource
    //endregion

    //region Backing data channels
    private val isLocationProviderAvailableMutableEvent = MutableLiveData<Event<LocationServiceState>>()
    private val ultravioletIndexMutableSource = MutableLiveData<UltravioletIndexState>()
    //endregion

    //region Observers
    private inner class LocationProviderAvailabilityObserver : DisposableSingleObserver<Boolean>() {

        override fun onSuccess(available: Boolean) {
            Timber.d("Location provider availability: $available")

            if (!available) {
                isLocationProviderAvailableMutableEvent.value = Event(LocationServiceState.Failure.LocationServiceOff)
            } else {
                // Access to location service is granted and location service is available, too
                isLocationProviderAvailableMutableEvent.value = Event(LocationServiceState.Available)
            }
        }

        override fun onError(e: Throwable) {
            Timber.e("Error: ${e.message}")
            isLocationProviderAvailableMutableEvent.value = Event(LocationServiceState.Failure.Error(e))
        }
    }

    private inner class UltravioletIndexObserver : DisposableSingleObserver<CurrentUltravioletIndex>() {

        override fun onSuccess(ultravioletIndex: CurrentUltravioletIndex) {
            Timber.d("UV index: ${ultravioletIndex.value}")
            ultravioletIndexMutableSource.value = UltravioletIndexState.Success(ultravioletIndex)
        }

        override fun onError(e: Throwable) {
            Timber.e("Error: ${e.message}")
            ultravioletIndexMutableSource.value = UltravioletIndexState.Failure(e)
        }
    }
    //endregion

    //region Public functions
    fun isLocationProviderAvailable() {
        isLocationProviderAvailableMutableEvent.value = Event(LocationServiceState.Loading)
        isLocationProviderAvailableUseCase.execute(LocationProviderAvailabilityObserver())
    }

    fun getUltravioletIndex() {
        if (isLocationProviderAvailableEvent.value?.peekContent() == LocationServiceState.Available) {
            // setValue() vs postValue()
            // * setValue() is called directly from caller thread and synchronously notifies observers.
            // Hence, this method must be called from the main thread.
            // * postValue() posts a task to a main thread to set the given value. Use it when you need
            // set a value from a background thread. If you called this method multiple times before a
            // main thread executed a posted task, only the last value would be dispatched.
            //
            // See: https://stackoverflow.com/questions/51299641/difference-of-setvalue-postvalue-in-mutablelivedata
            ultravioletIndexMutableSource.value = UltravioletIndexState.Loading
            getUltravioletIndexUseCase.execute(UltravioletIndexObserver())
        } else {
            Timber.e("Before asking for the ultraviolet index, ask for location service availability")
        }
    }
    //endregion

    override fun onCleared() {
        super.onCleared()

        isLocationProviderAvailableUseCase.dispose()
        getUltravioletIndexUseCase.dispose()
    }
}
