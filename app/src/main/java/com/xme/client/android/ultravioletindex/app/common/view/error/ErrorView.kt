/*
 * Copyright (C) 2019 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.app.common.view.error

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.xme.client.android.ultravioletindex.app.common.error.ErrorBundle
import com.xme.client.android.ultravioletindex.databinding.ViewErrorBinding
import timber.log.Timber
import java.lang.ref.WeakReference
import kotlin.DeprecationLevel.ERROR

/**
 * View used to display an error to the user.
 */
class ErrorView : FrameLayout {

    private val binding = ViewErrorBinding.inflate(LayoutInflater.from(context), this)

    var errorBundle: ErrorBundle? = null
        set(value) {
            field = value
            context?.let { context ->
                field?.let { errorBundle ->
                    binding.tvErrorViewText.text = context.getText(errorBundle.stringId)
                }
            }
        }

    // Write-only property
    // See: https://youtrack.jetbrains.com/issue/KT-6519
    private var _errorListenerWeakReference: WeakReference<ErrorListener>? = null

    @get:JvmSynthetic // Hide from Java callers
    var errorListener: ErrorListener
        @Deprecated("", level = ERROR) // Prevent Kotlin callers
        get() = throw UnsupportedOperationException()
        set(value) {
            _errorListenerWeakReference = WeakReference(value)
        }

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) :
            super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        binding.btErrorViewRetryButton.setOnClickListener {
            _errorListenerWeakReference?.let { weakReference ->
                weakReference.get()?.let { errorListener ->
                    errorBundle?.let { error ->
                        errorListener.onRetry(error.retryAction)
                    } ?: Timber.e("There is no error bundle")
                } ?: Timber.e("Error listener is null")
            } ?: Timber.e("There is no error listener")
        }
    }

    // Used to overwrite default message created when error bundled is set
    fun setErrorMessage(message: String) {
        binding.tvErrorViewText.text = message
    }
}