/*
 * Copyright (C) 2020 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.app.common.fragment

import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController

/**
 * Returns true if the navigation controller is still pointing at 'this' fragment, or false if it already navigated away.
 * See: https://medium.com/@ffvanderlaan/fixing-the-dreaded-is-unknown-to-this-navcontroller-68c4003824ce
 *      https://stackoverflow.com/a/56168225/5189200
 *      https://stackoverflow.com/a/61609489/5189200
 */
fun Fragment.mayNavigateTo(destinationId: Int): Boolean {

    val navController = findNavController()
    val currentId = navController.currentDestination?.id
    return currentId != destinationId
}