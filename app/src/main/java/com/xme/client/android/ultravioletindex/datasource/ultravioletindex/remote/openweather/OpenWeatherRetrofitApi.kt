/*
 * Copyright (C) 2019 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.datasource.ultravioletindex.remote.openweather

import com.xme.client.android.ultravioletindex.BuildConfig
import com.xme.client.android.ultravioletindex.datasource.ultravioletindex.remote.openweather.model.OneCall
import io.reactivex.rxjava3.core.Single
import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.Interceptor
import okhttp3.Interceptor.Chain
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Logger
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import timber.log.Timber

/**
 * Open Weather API implemented with Retrofit.
 */
interface OpenWeatherRetrofitApi {

    @GET("onecall")
    fun getWeather(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("exclude") exclude: String = DEFAULT_EXCLUSION_QUERY_PARAM
    ): Single<OneCall>

    companion object {
        private const val OPEN_WEATHER_APP_ID = BuildConfig.OPENWEATHER_APP_ID
        private const val BASE_URL = "https://api.openweathermap.org/data/"
        private const val API_VERSION = "2.5/"
        private const val API_KEY_QUERY_PARAM = "appid"
        private const val DEFAULT_EXCLUSION_QUERY_PARAM = "minutely,hourly,alerts"

        fun create(): OpenWeatherRetrofitApi =
            create(
                (BASE_URL + API_VERSION).toHttpUrlOrNull()!!
            )

        private fun create(httpUrl: HttpUrl): OpenWeatherRetrofitApi {
            val logger = HttpLoggingInterceptor(object : Logger {
                override fun log(message: String) {
                    Timber.d(message)
                }
            })
            logger.level = HttpLoggingInterceptor.Level.BODY

            val apiKeyInterceptor = object : Interceptor {
                override fun intercept(chain: Chain): Response {
                    val original = chain.request()
                    val originalHttpUrl = original.url

                    val url = originalHttpUrl.newBuilder()
                        .addQueryParameter(
                            API_KEY_QUERY_PARAM,
                            OPEN_WEATHER_APP_ID
                        )
                        .build()

                    // Request customization: add request headers
                    val requestBuilder = original.newBuilder()
                        .url(url)

                    val request = requestBuilder.build()

                    return chain.proceed(request)
                }
            }

            val client = OkHttpClient.Builder()
                .addInterceptor(apiKeyInterceptor)
                .addInterceptor(logger) // Place logger as last interceptor to log anything previous
                .build()

            return Retrofit.Builder()
                .baseUrl(httpUrl)
                .client(client)
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(OpenWeatherRetrofitApi::class.java)
        }
    }
}