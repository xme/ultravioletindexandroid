/*
 * Copyright (C) 2019 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.app.information.markwon.gif;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import io.noties.markwon.image.AsyncDrawable;
import io.noties.markwon.image.AsyncDrawableLoader;
import io.noties.markwon.image.ImageSize;
import io.noties.markwon.image.ImageSizeResolver;
import pl.droidsonroids.gif.GifDrawable;

public class GifAwareAsyncDrawable extends AsyncDrawable {

    public interface OnGifResultListener {
        void onGifResult(@NonNull GifAwareAsyncDrawable drawable);
    }

    private final Drawable gifPlaceholder;
    private OnGifResultListener onGifResultListener;
    private boolean isGif;

    public GifAwareAsyncDrawable(
            @NonNull Drawable gifPlaceholder,
            @NonNull String destination,
            @NonNull AsyncDrawableLoader loader,
            @Nullable ImageSizeResolver imageSizeResolver,
            @Nullable ImageSize imageSize) {
        super(destination, loader, imageSizeResolver, imageSize);
        this.gifPlaceholder = gifPlaceholder;
    }

    public void onGifResultListener(@Nullable OnGifResultListener onGifResultListener) {
        this.onGifResultListener = onGifResultListener;
    }

    @Override
    public void setResult(@NonNull Drawable result) {
        super.setResult(result);
        isGif = result instanceof GifDrawable;
        if (isGif && onGifResultListener != null) {
            onGifResultListener.onGifResult(this);
        }
    }

    @Override
    public void draw(@NonNull Canvas canvas) {
        super.draw(canvas);

        if (isGif) {
            final GifDrawable drawable = (GifDrawable) getResult();
            if (!drawable.isPlaying()) {
                gifPlaceholder.setBounds(drawable.getBounds());
                gifPlaceholder.draw(canvas);
            }
        }
    }
}
