/*
 * Copyright (C) 2018 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.datasource.ultravioletindex.local.sharedpreferences

import com.xme.client.android.ultravioletindex.data.ultravioletindex.source.CachedUltravioletIndexDataStore
import com.xme.client.android.ultravioletindex.data.util.JsonUtils
import com.xme.client.android.ultravioletindex.datasource.ultravioletindex.local.common.criteria.CacheCriterion
import com.xme.client.android.ultravioletindex.datasource.ultravioletindex.local.sharedpreferences.mapper.SharedPreferencesCurrentUltravioletIndexMapper
import com.xme.client.android.ultravioletindex.datasource.ultravioletindex.local.sharedpreferences.model.SharedPreferencesCurrentUltravioletIndex
import com.xme.client.android.ultravioletindex.model.ultravioletindex.CurrentUltravioletIndex
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

/**
 * Implementation of [CachedUltravioletIndexDataStore] using [android.content.SharedPreferences].
 */
class SharedPreferencesUltravioletIndexDataStore(
    private val sharedPreferencesHelper: SharedPreferencesHelper,
    private val cacheMapperSharedPreferences: SharedPreferencesCurrentUltravioletIndexMapper,
    private val cacheCriterion: CacheCriterion<CurrentUltravioletIndex>
) : CachedUltravioletIndexDataStore {

    override fun clearUltravioletIndex(): Completable {
        return Completable.defer {
            sharedPreferencesHelper.currentUltravioletIndex = ""
            Completable.complete()
        }
    }

    override fun saveUltravioletIndex(currentUltravioletIndex: CurrentUltravioletIndex): Completable {
        return Completable.defer {
            val cachedCurrentUltravioletIndex = cacheMapperSharedPreferences.mapToCached(currentUltravioletIndex)
            sharedPreferencesHelper.currentUltravioletIndex = JsonUtils.fromPojoToJson(cachedCurrentUltravioletIndex)
            Completable.complete()
        }
    }

    override fun getUltravioletIndex(latitude: Double, longitude: Double): Single<CurrentUltravioletIndex> {
        return Single.defer {
            // Use a data source mapper and a data defined model as done with location
            val cachedCurrentUltravioletIndex = JsonUtils.fromJsonToPojo(
                sharedPreferencesHelper.currentUltravioletIndex,
                SharedPreferencesCurrentUltravioletIndex::class.java
            )

            // TODO: If cached entity is null, return an error with an exception defined in this layer
            Single.just(cacheMapperSharedPreferences.mapFromCached(cachedCurrentUltravioletIndex!!))
        }
    }

    /**
     * Check whether there is an instance of [CurrentUltravioletIndex] stored in the cache
     * and it is not expired.
     */
    override fun isUltravioletIndexCached(latitude: Double, longitude: Double): Single<Boolean> {
        return Single.defer {
            getUltravioletIndex(latitude, longitude)
                .flatMap { currentUltravioletIndex: CurrentUltravioletIndex ->
                    cacheCriterion.isValidCache(currentUltravioletIndex, latitude, longitude)
                }
                .onErrorReturn { false }
        }
    }
}