/*
 * Copyright (C) 2018 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.data.util

import timber.log.Timber
import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime

object DateUtils {

    @JvmStatic
    fun isSameDay(instant1: Instant, instant2: Instant, timeZoneId: String): Boolean {
        var result = false

        try {
            val zoneId = ZoneId.of(timeZoneId)
            val zonedDateTime1 = ZonedDateTime.ofInstant(instant1, zoneId)
            val zonedDateTime2 = ZonedDateTime.ofInstant(instant2, zoneId)

            result = (zonedDateTime1.year == zonedDateTime2.year) &&
                    (zonedDateTime1.dayOfYear == zonedDateTime2.dayOfYear)
        } catch (e: Exception) {
            Timber.e("An error was found while comparing instants: ${e.message}")
        }

        return result
    }
}