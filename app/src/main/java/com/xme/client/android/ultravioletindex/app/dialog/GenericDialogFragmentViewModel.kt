/*
 * Copyright (C) 2020 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.app.dialog

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.xme.client.android.ultravioletindex.app.common.event.Event

class GenericDialogFragmentViewModel : ViewModel() {

    /**
     * Public property that is immutable (read only).
     *
     * This property is an event because the observer only wants to see the result once, after dismissing the dialog. If
     * the observer is rotated, and observer subscribes to the live data again, it doesn't want to see old results.
     */
    val genericDialogResultEvent: LiveData<Event<GenericDialogResult>>
        get() = genericDialogResultMutableEvent

    /**
     * Private property that it is mutable (read/write).
     */
    private val genericDialogResultMutableEvent = MutableLiveData<Event<GenericDialogResult>>()

    fun publish(result: GenericDialogResult) {
        genericDialogResultMutableEvent.value = Event(result)
    }
}