/*
 * Copyright (C) 2018 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.data.ultravioletindex.source

/**
 * Factory for creating an instance of a local or remote UltravioletIndexDataStore.
 */
class UltravioletIndexDataStoreFactory(
    private val cachedUltravioletIndexCacheIndexDataStore: CachedUltravioletIndexDataStore,
    private val ultravioletIndexRemoteIndexDataStore: UltravioletIndexDataStore
) {

    /**
     * Returns a data store based on whether or not there is content in the cache and the cache
     * has not expired.
     */
    fun retrieveDataStore(isCached: Boolean): UltravioletIndexDataStore {
        if (isCached) {
            return retrieveCacheDataStore()
        }
        return retrieveRemoteDataStore()
    }

    /**
     * Return an instance of the cache data store.
     */
    fun retrieveCacheDataStore(): CachedUltravioletIndexDataStore {
        return cachedUltravioletIndexCacheIndexDataStore
    }

    /**
     * Return an instance of the remote data store.
     */
    fun retrieveRemoteDataStore(): UltravioletIndexDataStore {
        return ultravioletIndexRemoteIndexDataStore
    }
}