/*
 * Copyright (C) 2019 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.app.common.view.index

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.provider.Settings
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.xme.client.android.ultravioletindex.R
import com.xme.client.android.ultravioletindex.app.chart.ChartFragmentDirections
import com.xme.client.android.ultravioletindex.app.common.LOCATION_PERMISSION
import com.xme.client.android.ultravioletindex.app.common.event.Event
import com.xme.client.android.ultravioletindex.app.common.fragment.mayNavigateTo
import com.xme.client.android.ultravioletindex.app.common.view.BaseFragment
import com.xme.client.android.ultravioletindex.app.dialog.GenericDialogFragmentViewModel
import com.xme.client.android.ultravioletindex.app.dialog.GenericDialogResult
import com.xme.client.android.ultravioletindex.app.dialog.GenericDialogResult.Cancelled
import com.xme.client.android.ultravioletindex.app.dialog.GenericDialogResult.Negative
import com.xme.client.android.ultravioletindex.app.dialog.GenericDialogResult.Positive
import com.xme.client.android.ultravioletindex.app.index.IndexFragmentDirections
import com.xme.client.android.ultravioletindex.app.index.dialog.DialogAction
import com.xme.client.android.ultravioletindex.app.index.dialog.DialogAction.OPEN_GOOGLE_MAPS
import com.xme.client.android.ultravioletindex.app.index.dialog.DialogAction.OPEN_LOCATION_SETTINGS
import com.xme.client.android.ultravioletindex.app.index.dialog.DialogAction.SHOW_BACKGROUND_LOCATION_SERVICE_RATIONALE
import com.xme.client.android.ultravioletindex.app.index.dialog.DialogAction.SHOW_LOCATION_SERVICE_RATIONALE
import timber.log.Timber

/**
 * This class was introduced to reduce duplication of code. However, I don't want to implement in this class any life
 * cycle method or use any view model, because I think it is much better to keep these things together in the same
 * class even at the cost of some duplication.
 *
 * TODO: Think about this design again and try to improve it.
 */
abstract class IndexBaseFragment : BaseFragment() {

    companion object {

        private const val REQUEST_LOCATION_PERMISSION = 0
        private const val REQUEST_BACKGROUND_LOCATION_PERMISSION = 1
    }

    private val genericDialogFragmentViewModel: GenericDialogFragmentViewModel by activityViewModels() // I haven't found a way to share this view model only between fragment and dialog

    //region Location provider availability
    protected fun fetchLocationProviderAvailability() {
        context?.let {
            // Check if the location permission has been granted
            if (ContextCompat.checkSelfPermission(it, LOCATION_PERMISSION) == PackageManager.PERMISSION_GRANTED) {
                Timber.v("Location permission is granted")
                isLocationProviderAvailable()

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    // Widget needs also the background location permission in Android 10 and, for the time being, I
                    // haven't found an easy way to ask for permissions in a widget, since there is no context there to
                    // receive results. Therefore, ask here about background location permission even if it not need by
                    // the app, only for the widget.
                    if (ContextCompat.checkSelfPermission(it, android.Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // Background location permission is NOT granted
                        if (shouldShowRequestPermissionRationale(android.Manifest.permission.ACCESS_BACKGROUND_LOCATION)) {
                            Timber.v("Rationale for background location permission should be shown")
                            askUser(
                                R.string.location_background_permission_rationale_title,
                                R.string.location_background_permission_rationale_message,
                                SHOW_BACKGROUND_LOCATION_SERVICE_RATIONALE.name
                            )
                        } else {
                            requestPermissions(arrayOf(android.Manifest.permission.ACCESS_BACKGROUND_LOCATION), REQUEST_BACKGROUND_LOCATION_PERMISSION)
                        }
                    }
                }
            } else {
                // Location permission is not granted and must be requested
                if (shouldShowRequestPermissionRationale(LOCATION_PERMISSION)) {
                    Timber.v("Rationale for location permission should be shown")
                    askUser(
                        R.string.location_permission_rationale_title,
                        R.string.location_permission_rationale_message,
                        SHOW_LOCATION_SERVICE_RATIONALE.name
                    )
                } else {
                    Timber.v("Permission is not available. Requesting permission")

                    // Do NOT request here background location permission, since "If you request a foreground location
                    // permission and the background location permission at the same time, the system ignores the
                    // request and doesn't grant your app either permission." in Android 11.
                    // See: https://developer.android.com/about/versions/11/privacy/location
                    requestPermissions(arrayOf(LOCATION_PERMISSION), REQUEST_LOCATION_PERMISSION)
                }
            }
        } ?: Timber.e("Context is null")
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_LOCATION_PERMISSION -> {
                // If request is cancelled, results array is empty
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Timber.v("Location permission has been granted")

                    // Location permission has been granted, so get ultraviolet index (even if background location
                    // permission is not granted, which is only needed for the widget).
                    isLocationProviderAvailable()
                    view?.let { view ->
                        Snackbar.make(view, "Location permission has been granted.", Snackbar.LENGTH_SHORT).show()
                    } ?: Timber.e("Fragment view is null")

                    // Now that foreground location permission is granted, ask for background location permission
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        askUser(
                            R.string.location_background_permission_rationale_title,
                            R.string.location_background_permission_rationale_message,
                            SHOW_BACKGROUND_LOCATION_SERVICE_RATIONALE.name
                        )
                    }
                } else {
                    // Location permission has been denied
                    Timber.v("Location permission has been denied")

                    view?.let { view ->
                        Snackbar.make(view, "Location permission has been denied.", Snackbar.LENGTH_SHORT).show()
                    } ?: Timber.e("Fragment view is null")
                }
            }
            REQUEST_BACKGROUND_LOCATION_PERMISSION -> {
                // If request is cancelled, results array is empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Timber.v("Background location permission has been granted")

                    // Background location permission has been granted
                    // There is nothing here to do, since this permission is used by the widget
                    view?.let { view ->
                        Snackbar.make(view, "Background location permission has been granted.", Snackbar.LENGTH_SHORT).show()
                    } ?: Timber.e("Fragment view is null")
                } else {
                    // Background location permission has been denied
                    Timber.v("Background location permission has been denied")

                    view?.let { view ->
                        Snackbar.make(view, "Background location permission has been denied.", Snackbar.LENGTH_SHORT).show()
                    } ?: Timber.e("Fragment view is null")
                }
            }
            // other 'case' lines to check for other
            // permissions this app might request.
            else -> Timber.w("A permission result with an unknown request code was received")
        }
    }

    /**
     * Function to be implemented in child classes with a view model.
     */
    abstract fun isLocationProviderAvailable()
    //endregion

    //region Location dialogs
    protected fun askUser(@StringRes title: Int, @StringRes message: Int, actionId: String) {
        // If a DialogFragment is shown and the screen is rotated, the parent fragment may try to show again the same
        // DialogFragment after rotation depending on view model state or other conditions. The problem will be that,
        // when the parent fragment tries to show the dialog again, the current top destination in navigation controller
        // is already the restored dialog, which does not include the navigation action used. In this case, a
        // "IllegalArgumentException: X is unknown to this NavController" will be thrown. The solution is either avoid
        // that situation, or checking first if the parent fragment is the current navigation component destination
        // (so, there are no other fragments on top of it).
        // For example, when location permission is not granted, index fragment shows a dialog, after rotation the
        // ultraviolet index is still null, thus triggering again get the index and, consequently, showing another
        // dialog about permissions by the index fragment. However, this time the index fragment will throw an
        // exception, because it is already behind a recovered dialog and the navigation controller cannot go to the
        // specified destination from the dialog, only from the fragment.
        // See: https://medium.com/@ffvanderlaan/fixing-the-dreaded-is-unknown-to-this-navcontroller-68c4003824ce
        if (mayNavigateTo(R.id.generic_dialog_fragment)) {
            // Check which is the fragment at the top of the navigation component to use the appropriate action
            val action = when (findNavController().currentDestination?.id) {
                R.id.index_fragment -> IndexFragmentDirections.actionIndexFragmentToGenericDialogFragment(
                    0,
                    title,
                    message,
                    false,
                    R.string.yes,
                    R.string.no,
                    actionId
                )
                R.id.chart_fragment -> ChartFragmentDirections.actionChartFragmentToGenericDialogFragment(
                    0,
                    title,
                    message,
                    false,
                    R.string.yes,
                    R.string.no,
                    actionId
                )
                else -> throw IllegalArgumentException("Unknown origin for dialog")
            }
            findNavController().navigate(action)
        }
    }

    protected fun listenToGenericDialog() {
        genericDialogFragmentViewModel.genericDialogResultEvent.observe(viewLifecycleOwner, Observer<Event<GenericDialogResult>> { event ->
            event.getContentIfNotHandled()?.let { result ->
                // Only proceed if the event has never been handled
                when (result) {
                    is Positive -> onGenericDialogPositiveAction(result.actionId)
                    is Negative -> onGenericDialogNegativeAction(result.actionId)
                    is Cancelled -> onGenericDialogCancelled(result.actionId)
                }
            }
        })
    }

    private fun onGenericDialogPositiveAction(action: String) {
        when (DialogAction.valueOf(action)) {
            OPEN_LOCATION_SETTINGS -> {
                context?.let { context ->
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    if (intent.resolveActivity(context.packageManager) != null) {
                        startActivity(intent)
                    }
                } ?: Timber.e("Context is null")
            }
            SHOW_LOCATION_SERVICE_RATIONALE -> {
                requestPermissions(arrayOf(LOCATION_PERMISSION), REQUEST_LOCATION_PERMISSION)
            }
            SHOW_BACKGROUND_LOCATION_SERVICE_RATIONALE -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    requestPermissions(arrayOf(android.Manifest.permission.ACCESS_BACKGROUND_LOCATION), REQUEST_BACKGROUND_LOCATION_PERMISSION)
                }
            }
            OPEN_GOOGLE_MAPS -> {
                val uri = "http://maps.google.com/maps"
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                intent.setPackage("com.google.android.apps.maps")
                startActivity(intent)
            }
        }
    }

    private fun onGenericDialogNegativeAction(action: String) {
        when (DialogAction.valueOf(action)) {
            SHOW_LOCATION_SERVICE_RATIONALE -> {
                Timber.d("User does not want to grant location permission to the app")
            }
            SHOW_BACKGROUND_LOCATION_SERVICE_RATIONALE -> {
                Timber.d("User does not want to grant background location permission to the app")
            }
            OPEN_LOCATION_SETTINGS -> {
                Timber.d("User does not want to open location settings screen")
            }
            OPEN_GOOGLE_MAPS -> {
                Timber.d("User does not want to open Google Maps for caching a location")
            }
        }
    }

    private fun onGenericDialogCancelled(action: String) {
        when (DialogAction.valueOf(action)) {
            SHOW_LOCATION_SERVICE_RATIONALE -> {
                Timber.d("Location rationale dialog was cancelled by user")
            }
            SHOW_BACKGROUND_LOCATION_SERVICE_RATIONALE -> {
                Timber.d("Background location rationale dialog was cancelled by user")
            }
            OPEN_LOCATION_SETTINGS -> {
                Timber.d("Open location dialog was cancelled by user")
            }
            OPEN_GOOGLE_MAPS -> {
                Timber.d("Open Google Maps dialog was cancelled by user")
            }
        }
    }
    //endregion
}
