/*
 * Copyright (C) 2018 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.domain.ultravioletindex.interactor

import com.xme.client.android.ultravioletindex.domain.interactor.SingleUseCase
import com.xme.client.android.ultravioletindex.domain.location.repository.LocationRepository
import com.xme.client.android.ultravioletindex.domain.ultravioletindex.repository.UltravioletIndexRepository
import com.xme.client.android.ultravioletindex.model.location.Location
import com.xme.client.android.ultravioletindex.model.location.exception.NoLocationAvailableException
import com.xme.client.android.ultravioletindex.model.ultravioletindex.CurrentUltravioletIndex
import com.xme.client.android.ultravioletindex.model.ultravioletindex.exception.UviNotComputableException
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers

/**
 * Use case for retrieving ultraviolet index for the mobile device current location using
 * [LocationRepository] and [UltravioletIndexRepository].
 */
class GetUltravioletIndexUseCase(private val locationRepository: LocationRepository, private val ultravioletIndexRepository: UltravioletIndexRepository) :
    SingleUseCase<CurrentUltravioletIndex, Void>() {

    override fun buildUseCaseObservable(params: Void?): Single<CurrentUltravioletIndex> {
        return locationRepository.getLastLocation() // Get last cached location, which may not exists after switching off/on location service, since that action clears cache
            .switchIfEmpty(Single.defer {
                // If the is no cached location, request location updates (which forces to retrieve a location in the system)
                locationRepository.startLocationUpdates().firstOrError() // Transform the observable into a single
                    .onErrorResumeNext {
                        // If the observable completes without emitting a first value or an error is thrown (which is currently not possible), thrown a NoLocationAvailableException
                        Single.error { throw NoLocationAvailableException() }
                    }.doFinally {
                        // Either finishing on success or on error, stop location updates
                        locationRepository.stopLocationUpdates()
                    }
            })
            .observeOn(Schedulers.io()) // getLastLocation() always returns result in main thread! This hack is needed because subscribe on background thread will not be respected by getLastLocation()
            .flatMap { location: Location -> ultravioletIndexRepository.getUltravioletIndex(location.latitude, location.longitude) }
            .map { currentUltravioletIndex ->
                if (!currentUltravioletIndex.isUviComputable()) {
                    throw UviNotComputableException(
                        "UV index not computable",
                        currentUltravioletIndex.latitude,
                        currentUltravioletIndex.longitude,
                        currentUltravioletIndex.sunriseZonedDateTime,
                        currentUltravioletIndex.sunsetZonedDateTime
                    )
                }
                currentUltravioletIndex
            }
    }
}