/*
 * Copyright (C) 2019 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.app.index.error

import com.xme.client.android.ultravioletindex.R
import com.xme.client.android.ultravioletindex.app.common.error.ErrorBundle
import com.xme.client.android.ultravioletindex.app.common.error.ErrorBundleBuilder
import com.xme.client.android.ultravioletindex.app.common.error.RetryAction
import com.xme.client.android.ultravioletindex.app.common.error.RetryAction.GetLastLocation
import com.xme.client.android.ultravioletindex.app.common.error.RetryAction.GetUltravioletIndex
import com.xme.client.android.ultravioletindex.app.common.error.RetryAction.RefreshWidget
import com.xme.client.android.ultravioletindex.app.common.exception.LocationServiceNoPermissionException
import com.xme.client.android.ultravioletindex.model.location.exception.NoLocationAvailableException
import com.xme.client.android.ultravioletindex.model.ultravioletindex.exception.UviNotComputableException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

/**
 * Factory for creating [ErrorBundle]s from [Throwable]s.
 */
class IndexErrorBundleBuilder : ErrorBundleBuilder {

    override fun build(throwable: Throwable, retryAction: RetryAction): ErrorBundle {
        val stringId: Int
        // Unwrap RuntimeExceptions, wrapped by RxJava emitter.tryOnError() function
        val t = if (throwable is RuntimeException && throwable.cause != null && throwable.cause is Exception) throwable.cause as Exception else throwable

        stringId = when (t) {
            // Map common/general errors
            is UnknownHostException -> R.string.error_no_network_connection
            is SocketTimeoutException -> R.string.error_no_network_connection
            else -> // Map by action
                when (retryAction) {
                    GetLastLocation -> when (t) {
                        // Map action errors
                        is LocationServiceNoPermissionException -> R.string.error_no_location_permission
                        is NoLocationAvailableException -> R.string.error_no_location
                        else -> R.string.error_get_last_known_location
                    }
                    GetUltravioletIndex -> when (t) {
                        // Map action errors
                        is NoLocationAvailableException -> R.string.error_no_location
                        is UviNotComputableException -> R.string.error_uvi_not_computable
                        else -> R.string.error_get_ultraviolet_index
                    }
                    RefreshWidget -> R.string.error_refresh_widget
                }
        }

        return ErrorBundle(t, retryAction, stringId)
    }
}
