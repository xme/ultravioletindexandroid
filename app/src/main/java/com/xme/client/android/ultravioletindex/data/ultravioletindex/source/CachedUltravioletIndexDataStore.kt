/*
 * Copyright (C) 2019 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.data.ultravioletindex.source

import com.xme.client.android.ultravioletindex.model.ultravioletindex.CurrentUltravioletIndex
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

/**
 * Interface defining methods for the cache data operations related to current ultraviolet index.
 * This interface is always an extension of the non-cached data store.
 */
interface CachedUltravioletIndexDataStore : UltravioletIndexDataStore {

    fun clearUltravioletIndex(): Completable

    fun saveUltravioletIndex(currentUltravioletIndex: CurrentUltravioletIndex): Completable

    fun isUltravioletIndexCached(latitude: Double, longitude: Double): Single<Boolean>
}