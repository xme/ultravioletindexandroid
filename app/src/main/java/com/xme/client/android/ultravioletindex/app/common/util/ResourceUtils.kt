/*
 * Copyright (C) 2018 Xavier Mellado Esteban
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xme.client.android.ultravioletindex.app.common.util

import android.content.Context
import androidx.annotation.ColorRes
import androidx.annotation.IntRange
import com.xme.client.android.ultravioletindex.R

object ResourceUtils {

    @JvmStatic
    fun fromUltravioletIndexToImageContents(context: Context, @IntRange(from = 0) uvi: Int?): UltravioletImageContents {
        return when (uvi) {
            null -> UltravioletImageContents(R.drawable.ic_uvi_error, context.getString(R.string.uvi_error))
            in Int.MIN_VALUE..-1 -> UltravioletImageContents(R.drawable.ic_uvi_error, context.getString(R.string.uvi_error))
            0 -> UltravioletImageContents(R.drawable.ic_night, context.getString(R.string.uvi_zero))
            1 -> UltravioletImageContents(R.drawable.ic_uvi_01, context.getString(R.string.uvi_template, uvi))
            2 -> UltravioletImageContents(R.drawable.ic_uvi_02, context.getString(R.string.uvi_template, uvi))
            3 -> UltravioletImageContents(R.drawable.ic_uvi_03, context.getString(R.string.uvi_template, uvi))
            4 -> UltravioletImageContents(R.drawable.ic_uvi_04, context.getString(R.string.uvi_template, uvi))
            5 -> UltravioletImageContents(R.drawable.ic_uvi_05, context.getString(R.string.uvi_template, uvi))
            6 -> UltravioletImageContents(R.drawable.ic_uvi_06, context.getString(R.string.uvi_template, uvi))
            7 -> UltravioletImageContents(R.drawable.ic_uvi_07, context.getString(R.string.uvi_template, uvi))
            8 -> UltravioletImageContents(R.drawable.ic_uvi_08, context.getString(R.string.uvi_template, uvi))
            9 -> UltravioletImageContents(R.drawable.ic_uvi_09, context.getString(R.string.uvi_template, uvi))
            10 -> UltravioletImageContents(R.drawable.ic_uvi_10, context.getString(R.string.uvi_template, uvi))
            else -> UltravioletImageContents(R.drawable.ic_uvi_11, context.getString(R.string.uvi_eleven_or_more))
        }
    }

    @JvmStatic
    @ColorRes
    fun fromUltravioletIndexToColorResourceId(@IntRange(from = 0) uvi: Int?): Int {
        return when (uvi) {
            null -> R.color.uvi_error
            in Int.MIN_VALUE..-1 -> R.color.uvi_error
            in 0..2 -> R.color.uvi_low
            in 3..5 -> R.color.uvi_moderate
            in 6..7 -> R.color.uvi_high
            in 8..10 -> R.color.uvi_very_high
            else -> R.color.uvi_extreme
        }
    }
}