<h1 align="center">
  <a href="https://gitlab.com/xme/ultravioletindexandroid"><img src="file:///android_asset/logo.png" alt="Ultraviolet index Android app" width="256"></a>
  <br>
  <br>
  Ultraviolet Index
  Android App
  <br>
  <br>
</h1>

<h4 align="center">A minimal mobile app for obtaining the <a href="http://www.who.int/uv" target="_blank">ultraviolet index</a> at your current location.</h4>

<br> 
<p align="center">
  <a href="#introduction">Introduction</a> •
  <a href="#ultraviolet-index">Ultraviolet index</a> •
  <a href="#ultraviolet-index-app">Ultraviolet index app</a> •
  <a href="#acknowledgments">Acknowledgments</a>
</p>
<br>

## Introduction

**Ultraviolet Index** is a open source and free app that allows you to know the ultraviolet index at your current location. The app has two components: the app itself and an [app widget](https://developer.android.com/guide/topics/appwidgets/overview) for your home screen.

The [repository](https://gitlab.com/xme/ultravioletindexandroid) contains the source code and technical documentation (README.md file). Here, you will find information about the ultraviolet index and how this app works.

## Ultraviolet index

### What is ultraviolet radiation?

From the [World Health Organization](https://www.who.int/uv/uv_and_health/en):

>Ultraviolet (UV) radiation is part of the electromagnetic spectrum emitted by the sun. Whereas UVC rays (wavelengths of 100-280 nm) are absorbed by the atmospheric ozone, most radiation in the UVA range (315-400 nm) and about 10 % of the UVB rays (280-315 nm) reach the Earth’s surface. Both UVA and UVB are of major importance to human health.
>
>Small amounts of UV are essential for the production of vitamin D in people, yet overexposure may result in acute and chronic health effects on the skin, eye and immune system.


### What is ultraviolet index?

From its [definition](https://en.wikipedia.org/wiki/Ultraviolet_index) at the Wikipedia:

>The ultraviolet index or UV Index is an international standard measurement of the strength of sunburn-producing ultraviolet (UV) radiation at a particular place and time.
>[...]
>
>The UV Index is designed as an open-ended linear scale, directly proportional to the intensity of UV radiation that causes sunburn on human skin. For example, if a light-skinned individual (without sunscreen) begins to sunburn in 30 minutes at UV Index 6, then that individual should expect to sunburn in about 15 minutes at UV Index 12 – twice the UV, twice as fast.
>[...]
>
>The purpose of the UV Index is to help people effectively protect themselves from UV radiation, which has health benefits in moderation but in excess causes sunburn, skin aging, DNA damage, skin cancer, immunosuppression, and eye damage such as cataracts [...]. Public health organizations recommend that people protect themselves (for example, by applying sunscreen to the skin and wearing a hat and sunglasses) if they spend substantial time outdoors when the UV Index is 3 or higher.

### What measures do you need to take in order to protect from UV radiation?

Source [Wikipedia](https://en.wikipedia.org/w/index.php?title=Ultraviolet_index&section=4#Index_usage):

| UV Index | Exposure level | Time to burn | Recommended protection |
| :------: | :------------: | :----------: | ---------------------- |
| 0 to 2   | Low            | 60 min       | A UV index reading of 0 to 2 means low danger from the Sun's UV rays for the average person. <br> Wear sunglasses on bright days. If you burn easily, cover up and use broad spectrum SPF 30+ sunscreen. Bright surfaces, such as sand, water, and snow, will increase UV exposure. |
| 3 to 5   | Moderate       | 45 min       | A UV index reading of 3 to 5 means moderate risk of harm from unprotected Sun exposure. <br> Stay in shade near midday when the Sun is strongest. If outdoors, wear Sun protective clothing, a wide-brimmed hat, and UV-blocking sunglasses. Generously apply broad spectrum SPF 30+ sunscreen every 2 hours, even on cloudy days, and after swimming or sweating. Bright surfaces, such as sand, water, and snow, will increase UV exposure. |
| 6 to 7   | High           | 30 min       | A UV index reading of 6 to 7 means high risk of harm from unprotected Sun exposure. Protection against skin and eye damage is needed. <br> Reduce time in the Sun between 10 a.m. and 4 p.m. If outdoors, seek shade and wear Sun protective clothing, a wide-brimmed hat, and UV-blocking sunglasses. Generously apply broad spectrum SPF 30+ sunscreen every 2 hours, even on cloudy days, and after swimming or sweating. Bright surfaces, such as sand, water, and snow, will increase UV exposure. |
| 8 to 10  | Very high      | 15 min       | A UV index reading of 8 to 10 means very high risk of harm from unprotected Sun exposure. Take extra precautions because unprotected skin and eyes will be damaged and can burn quickly. <br> Minimize Sun exposure between 10 a.m. and 4 p.m. If outdoors, seek shade and wear Sun protective clothing, a wide-brimmed hat, and UV-blocking sunglasses. Generously apply broad spectrum SPF 30+ sunscreen every 2 hours, even on cloudy days, and after swimming or sweating. Bright surfaces, such as sand, water, and snow, will increase UV exposure. |
| 11+      | Extreme        | 10 min       | A UV index reading of 11 or more means extreme risk of harm from unprotected Sun exposure. Take all precautions because unprotected skin and eyes can burn in minutes. <br> Try to avoid Sun exposure between 10 a.m. and 4 p.m. If outdoors, seek shade and wear Sun protective clothing, a wide-brimmed hat, and UV-blocking sunglasses. Generously apply broad spectrum SPF 30+ sunscreen every 2 hours, even on cloudy days, and after swimming or sweating. Bright surfaces, such as sand, water, and snow, will increase UV exposure. |

When interpreting the UV index and recommendations, be aware that:

- The intensity of UV radiation reaching the surface of the earth depends on the angle of the Sun in the sky. Each day, the Sun achieves its highest angle (highest intensity, shortest shadows) at solar noon, which approximately corresponds to 12:00, or 13:00 (1:00 PM) during daylight saving time. This is because of the differences between solar time and local time in a given time zone as well as Earth's elliptical orbit causing the exact time of solar noon to vary seasonally within several minutes. UV risk is high when the Sun is directly enough overhead that people's shadows are shorter than their height (greater than 45°).
- UV intensity can nearly double with reflection from snow or other bright surfaces like water, sand, or concrete.
- The recommendations given are for average adults with lightly tanned skin. Those with darker skin are more likely to withstand greater Sun exposure, while extra precautions are needed for children, seniors, particularly fair-skinned adults, and those who have greater Sun sensitivity for medical reasons or from UV exposure in previous days. The skin's recovery from UV radiation generally takes two days or more to run its course.
- Because of the way the UV index is calculated, it technically expresses the risk of developing sunburn, which is caused mostly by UVB radiation. However, UVA radiation also causes damage (photoaging, melanoma). Under some conditions, including most tanning beds which generate even higher UV intensities, the UVA level may be disproportionately higher than described by the UV index. The use of broad-spectrum (UVA/UVB) sunscreen can help address this concern.

More sources and information in:

- [Global Solar UV Index: A Practical Guide](https://www.who.int/uv/publications/en/UVIGuide.pdf)
- [A Guide To The UV Index](https://www.epa.gov/sites/production/files/documents/uviguide.pdf)
- [United States Environmental Protection Agency Sun Safety](https://www.epa.gov/sunsafety/uv-index-scale-0)

## Ultraviolet index app

### Purpose

The main purpose of this app is to show you the UV Index at any point in time, giving you the maximum UV index, which is what most weather applications do, and predicting how it will change along the day.

### What makes this app different?

In general, weather applications have two downsides: first, the give you **only the maximum UV Index** for the solar midday, but the do not show you how the index changes along time; second, they consume a lot of resources requesting weather forecasts, waking up your phone and using much more memory and space than the strictly necessary.

I assure you I will do my best to make this app as **fast, light and good citizen in your phone** as I can.

Besides, this app is open source, you can review its code, and totally free with no ads.

### Components

This app is divided into two components: the app itself and the home widget.

#### The app

The app is what you are using right now. It is a simple application with three sections:

1. The current UV Index with some associated information, like maximum (solar midday) value, date, sunrise and sunset time, etc.
2. An UV Index graph with its variation along the day.
3. This information page.

#### The widget

The widget can be placed in your home screen in order to quickly know the current UV Index. Just add the widget to the home screen, and it will be automatically updated every 30 minutes.

##### Add a widget

1. On a Home screen, touch and hold an empty space.
2. Tap Widgets <img src="file:///android_asset/ic_widgets_black.png" alt="widgets" width="64">.
3. Touch and hold a widget. You’ll see images of your Home screens.
4. Slide the widget to where you want it. Lift your finger.

Tip: When apps come with widgets, touch and hold the app icon, then tap Widgets <img src="file:///android_asset/ic_widgets_black.png" alt="widgets" width="64">.

##### Resize a widget

1. Touch and hold the widget on your Home screen.
2. Lift your finger. If the widget can be resized, you'll see an outline with dots on the sides.
3. To resize the widget, drag the dots.
4. When you're done, tap outside the widget.

### What are the app limitations?

This app has several limitations, so it is worth to take them into account:

1. Weather services provide only maximum UV Index, so variation along the day must be estimated. See section <a href="#ultraviolet-index-estimation">Ultraviolet index estimation</a>
2. Weather services have request quotas, so the app may not work properly if quota is exceeded due to a high number of users, something that will not likely happen :stuck_out_tongue_winking_eye:. Current provider [OpenWeatherMap](https://openweathermap.org/) has 60 calls per minute with a free account.
3. Some weather services, such as OpenWeatherMap, return UV Index based on location but they do not take into account time zones, and that makes their services almost useless. See section <a href="#location-and-time-zone-problem">Location and time zone problem</a>.
4. The mobile device default time zone is used and cannot be changed, so that if latitude and longitude are New York, but local time is in Beijing, the result will have no sense. In addition and related to previous situation, this app assumes that sunrise and sunset events occur in the same day, that is: a) sunrise date is equal to sunset date and b) 00:00 <= sunrise time <= sunset time <= 23:59, when these conditions are not meet, an error will be shown asking you to check latitude, longitude and time zone. If you live in a location where this precondition is not meet, I am really sorry but, for the time being, this app cannot help you. 
5. In order to use as fewer resources as possible, this app just asks for the last known location in your device. Last known location is available only if **another** app has requested the device location previously. Therefore, if no other app has requested locations, Android will return no locations to Ultraviolet Index. When this is detected, the app ask you to open Google Maps, so that it can retrieve a cached location. Just open Maps, wait until the position marker is shown and close it. Ultraviolet index should work fine afterwards.
6. Last known location acquisition time is not checked, so very old and likely invalid locations may be used, when they should be filtered.
            
Some of these limitations can be fixed, and I plan to do that. Meanwhile, bear them in mind when you interact with the app or the widget.

### Ultraviolet index estimation

Weather services usually provide only maximum UV Index for solar noon. This app estimates the value for a given instant as

*f(x) = maxUVI · sin(x)*

where

- *x* is a time value inside the interval *0 <= x <= π*, *0* denoting sunrise time and *π* denoting sunset time.
- *maxUVI* is maximum UV index for solar noon.

This function generates a bell shape function that approximates variation along the day.

### Location and time zone problem

As far as I know, when a location is send to a weather service for obtaining a prediction, the service should compute which time zone is associated to the provided location, in order to properly compute which day is and return the right prediction.

In OpenWeatherMap, when a request is received the [Coordinated Universal Time](https://en.wikipedia.org/wiki/Coordinated_Universal_Time) (or UTC) for Greenwich Mean (UTC+0h) is used to return the result. This approach works if you live, for example, in London, but it doesn't work if you live in Sydney, since a request early morning in Sidney (UTC+11h) is a day less in UTC+0h and the returned result is not for the current date, but for the previous date in local time.

## Acknowledgments

Night icon made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>.

[//]: # (This is a comment)