package com.xme.client.android.ultravioletindex.common.log

import android.content.Context
import com.xme.client.android.ultravioletindex.TimberLog

object TimberLogImplementation : TimberLog {

    override fun init(context: Context) {
        TimberUtils.plantDebugTree()
    }

    override fun init(context: Context, userId: String) {
        init(context)
    }
}
