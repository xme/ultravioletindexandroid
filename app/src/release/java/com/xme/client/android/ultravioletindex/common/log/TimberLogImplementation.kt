package com.xme.client.android.ultravioletindex.common.log

import android.content.Context
import android.content.pm.ApplicationInfo
import com.xme.client.android.ultravioletindex.TimberLog
import com.xme.client.android.ultravioletindex.common.log.helpers.CrashLibraryTree
import timber.log.Timber

object TimberLogImplementation : TimberLog {


    override fun init(context: Context) {
        init(context, "")
    }

    override fun init(context: Context, userId: String) {
        Timber.plant(CrashLibraryTree(userId))
        // Check whether release app has the "android:debuggable" flag set to true in the manifest
        if ((context.applicationInfo.flags and ApplicationInfo.FLAG_DEBUGGABLE) != 0) {
            // If debuggable, plant the tree to log to logcat, too
            TimberUtils.plantDebugTree()
        }
    }
}