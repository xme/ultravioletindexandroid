<h1 align="center">
  <a href="https://gitlab.com/xme/ultravioletindexandroid"><img src="assets/img/logo.png" alt="Ultraviolet index Android app" width="256"></a>
  <br>
  <br>
  Ultraviolet Index Android App
  <br>
  <br>
</h1>

<h4 align="center">A minimal mobile app for obtaining the <a href="http://www.who.int/uv" target="_blank">ultraviolet index</a> at your current location.</h4>

<br> 
<p align="center">
  <a href="#introduction">Introduction</a> •
  <a href="#getting-started">Getting Started</a> •
  <a href="#dependencies">Dependencies</a> •
  <a href="#prerequisites">Prerequisites</a> •
  <a href="#installing">Installing</a> •
  <a href="#built-with">Built With</a> •
  <a href="#contributing">Contributing</a> •
  <a href="#versioning">Versioning</a> •
  <a href="#authors">Authors</a> •
  <a href="#license">License</a> •
  <a href="#acknowledgments">Acknowledgments</a>
</p>
<br>

## Introduction

**Ultraviolet Index** is a small app that allows you know the [ultraviolet index](href="http://www.who.int/uv") at your current location. The app has two components: the app itself and an [app widget](https://developer.android.com/guide/topics/appwidgets/overview) for your home screen.

If you like this project, :star: star it on GitLab, please. It will help me!

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Dependencies

This app uses:

* [Android X](https://developer.android.com/jetpack/androidx/), which provides backwards-compatibility across Android releases
* [Android Architecture Components](https://developer.android.com/topic/libraries/architecture/)
   * [LiveData](https://developer.android.com/topic/libraries/architecture/livedata)
   * [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel)
   * [Navigation](https://developer.android.com/guide/navigation)
* [Material Components](https://material.io/develop/android/docs/getting-started/)
* [RxJava](https://github.com/ReactiveX/RxJava), Java implementation of API for asynchronous programming with observable streams
* [Retrofit](https://square.github.io/retrofit/), a type-safe HTTP client for Android and Java
* [Timber](https://github.com/JakeWharton/timber), a logger with a small, extensible API which provides utility on top of Android's normal Log class
* [LeakCanary](https://github.com/square/leakcanary), a memory leak detection library for Android and Java
* [R8](https://developer.android.com/studio/build/shrink-code) in order to shrink, obfuscate, and optimize the app.
* [Crashlytics](https://firebase.google.com/docs/crashlytics), a realtime crash reporter.
* [Markwon](https://github.com/noties/Markwon), a markdown library for Android.

### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system


## Built With

* [Android Studio](https://developer.android.com/studio/) IDE.
* [grandcentrix GmbH Android Code Style](https://github.com/grandcentrix/AndroidCodeStyle) with official Kotlin code style.
* [Gradle Versions Plugin](https://github.com/ben-manes/gradle-versions-plugin) that provides a task to determine which dependencies have updates.

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on code of conduct, and the process for submitting pull requests.

## Versioning

I use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/xme/ultravioletindexandroid/tags).
For the changes introduced on each version, see the [CHANGELOG.md](CHANGELOG.md) file.

## Authors

* **Xavier Mellado Esteban** - [XME](https://gitlab.com/xme)

## License

This project is licensed under the Apache 2.0 License - see the [LICENSE](LICENSE) file for details.

    Copyright (C) 2018 Xavier Mellado Esteban
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

## Acknowledgments

* [Guillermo Aguilar](https://gitlab.com/gcaguilar) for pushing me to learn Kotlin.
* [Android 10 Clean Architecture](https://github.com/android10/Android-CleanArchitecture).
* Skeen, Josh, and David Greenhalgh. *[Kotlin Programming: the Big Nerd Ranch Guide](https://www.bignerdranch.com/books/kotlin-programming/)*. Big Nerd Ranch, 2018.
* [Android Clean Architecture Components Boilerplate (MVVM version)](https://github.com/bufferapp/clean-architecture-koin-boilerplate).
* [Android Architecture Components samples](https://github.com/googlesamples/android-architecture-components).
* [Android Architecture Blueprints](https://github.com/googlesamples/android-architecture).
* Sandro Mancuso. 2014. The Software Craftsman: Professionalism, Pragmatism, Pride (Robert C. Martin Series) (1st ed.). Prentice Hall Press, Upper Saddle River, NJ, USA.
* [Android Notifications Sample](https://github.com/googlesamples/android-Notifications).
* [Android Clean Architecture Components Boilerplate (MVP version)](https://github.com/bufferapp/android-clean-architecture-boilerplate).

Night icon made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>.

[//]: # (This is a comment)