# To do list

### App:

- [ ] Add testing.
- [ ] Obtain location using and intent (Google Maps).
- [ ] If last known location is older than a given amount of time, request a location.
- [ ] Check network connection at the beginning?
- [ ] Remove limitation of sunrise and sunset in the same day. Sunlight can be more than 24 hours.
- [ ] If cached location is older than a given amount of time, the location is not valid and a new one should be retrieved.
- [ ] Build a decent UI.
- [ ] Widget update trigger should be smarter and execute about one or two minutes past o'clock to reduce differences with the data shown in the app.
- [x] Migrate from OpenWeather UVI API (deprecated) to the new OpenWeather One Call API.
- [x] Fix widget size.
- [x] Add privacy and terms to the app.
- [x] Publish the app as is on Google Play as a demo.
- [x] Fix Material Design problems and add dark theme support.
- [x] Kotlin Android Extensions are deprecated and they should be replaced for Android Jetpack View Binding.
- [x] Update to Android 11 (new location permission management).
- [x] Log to logcat in release mode if debuggable flag is set to true.
- [x] If ultraviolet index cannot be obtained due to permissions not granted or location service off, show an empty screen.
- [x] Fix chart fragment crash when location permission is not granted.
- [x] View models life should be tied to the minimum scope required (activity, navigation graph, etc.) where they are used.
- [x] View models should not expose a *mutable* live data, otherwise consumers can erroneously publish results. A view model should always expose a read only property, a live data.
- [x] Show dialogs using Navigation component and communicate dialog and fragment with a shared view model.
- [x] Test app and widget in Android 10 (access to device location in the background).
- [x] Add desugaring to get access to Java 8 language features and APIs in Android Studio 4. Remove ThreeTen Android Backport library from the project and use directly Java 8 Time API instead.
- [x] OnActivityCreated() was deprecated.
- [x] Fix warnings and deprecations.
- [x] Migrate to RxJava 3.
- [x] Migrate to Firebase Crashlytics.
- [X] If last known location is not available, request a location.
- [x] Build an specific error for the situation where UV index estimation cannot be computed.
- [x] Implement information UI.
- [x] Implement chart UI.
- [x] Implement index UI.
- [x] Add bottom navigation with navigation component.
- [x] Add bottom navigation with three fragments: index, chart, information.
- [x] Use navigation component to manage app navigation and fragment transition.
- [x] Avoid Retrofit HttpExceptions to get outside data source layer.
- [x] Make fragment show dialogs and receive their results.  
- [x] Convert listeners to weak references.
- [x] Add a splash screen theme.
- [x] Convert loading view to a custom view.
- [x] Add vertical model layer.
- [x] Remove duplication created by data source api interfaces.
- [x] Remove duplication in data store interfaces.
- [x] Use R8.
- [x] Basic error management and error view.
- [x] Live data must not be observed in initializeContents() using *this* as LifecycleOwner, otherwise they are registered several times when the screen is rotated but never unregistered. Fragment getViewLifecycleOwner() must be used as LifecycleOwner.
- [x] Early initialization of views must be placed iin onViewCreated() when kotlin extensions are available to access UI elements by id.
- [x] Do not hold the fragment view inside the fragment, it may cause memory leaks (especially if the fragment is retained!).
- [x] Remove retain instance from fragments. Fragments should never be retained, because state is stored in view model. Only retain instances for creating headless fragments in order to execute long running tasks or store state.
- [x] Basic loading management and loading view.
- [x] Build view state with Kotlin sealed classes.
- [x] Create an app logo and icon.

### Widget:

- [ ] Add an activity to configure widget? Take into account that phantom widgets may appear, see this [post](https://stackoverflow.com/a/17387978/5189200).
- [ ] Test thoroughly the widget.
- [x] Make the widget compatible with Google TalkBack.
- [x] Update notification icon and set its color.
- [x] Refresh widget when the app gets new results.
- [x] Fix widget update time to one minute when debugging and one hour in release. For the time being, alarm manager will be used only in debug.
- [x] Intent for alarm manager may retrieve widget list from the system, instead of trying to store and update it in shared preferences.
- [x] Add a button in the app to force a widget update.
- [x] Click on widget opens app.
- [x] Change widget view depending on state.
- [x] Create widget icons for UVI and states (no location permission, no location service, loading, etc.).
- [x] Test if location service is available in widget update service. If not, send a notification to let the user know, with an action to open location settings.
- [x] Test if location permission is granted in widget update service. If not, send a notification to let the user know with an action to open the app.

### Project:

- [x] Add Crashlytics.
- [x] Add Canary Leak.
- [x] Update dependencies and add Gradle Versions Plugin to manage dependency updates.
- [x] Update [grandcentrix GmbH Android Code Style](https://github.com/grandcentrix/AndroidCodeStyle), add it to readme file.

### Port Java app to Kotlin:

- [x] Split widget in three parts: a) update management, b) data retrieval, c) view update.
- [x] Port widget to Kotlin.
- [x] Add widget to home screen.
- [x] Compute distance between two geo positions and use it to retrieve cached data or fresh data.
- [x] Cache results. Add current ultraviolet index entity in data and map to domain. Refactor/relocate classes and make all mappers implement one of the mapper interfaces.
- [x] If app have location permission and location service is off, show a dialog to open location settings.
- [x] Add a generic dialog for asking questions to the user.
- [x] Get mobile device location and manage location permission.
- [x] Move all UI margins to dimens file.
- [x] Add a button to get UV index and write to TextView. Rotation should be supported.
- [x] Add date utils class.
- [x] Add JSON utils class to de/serialize instances.
- [x] Add ThreeTen Android Backport library for using Java 8 Time API in Android.
- [x] Create a framework layer for removing Android dependencies from data layer.
- [x] Add a simple injection mechanism.
- [x] Wrap get ultraviolet index operation in a use case.
- [x] Add network call to OpenWeather provider.

### Initial project:

- [x] Improve Gradle files defining dependencies at the top level in a single place.
- [x] Add licensing to every file. Configure Android Studio to add licensing to new files.
- [x] Migrate project to AndroidX.
- [x] Add code style.
- [x] Add README file.
- [x] Add LICENSE file.
- [x] Generate initial project with Android Studio 3.2. Use fragment with ViewModel template.