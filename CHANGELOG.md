# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

[//]: # (## [X.X.X] - YYYY-MM-DD)
[//]: # (### Added)
[//]: # (- Add new features)
[//]: # (### Changed)
[//]: # (- Add breaking changes first!)
[//]: # (- Add changes in existing functionality)
[//]: # (### Deprecated)
[//]: # (- Add soon-to-be removed features)
[//]: # (### Removed)
[//]: # (- Add now removed features)
[//]: # (### Fixed)
[//]: # (- Add any bug fixes)
[//]: # (### Security)
[//]: # (- Add vulnerabilities)

## [Unreleased]
### Added

### Changed

### Fixed

## [0.2.2] - 2021-05-01
### Added
- Privacy and terms legal texts were added.

### Changed
- OpenWeather UVI API (deprecated) was exchanged for the new OpenWeather One Call API.

### Fixed
- Widget size was fixed and resize behavior was added.

## [0.2.0] - 2021-04-05
### Added
- The app was published as a demo on Google Play.
- Support for dark theme was implemented, and Material Design problems were fixed.
- Dialogs are shown using Navigation component.
- Location updates are requested until the first result is found when there is no cached location.
- An error for the situation where UV index estimation cannot be computed was added.
- Information screen was implemented.
- Chart screen was implemented.
- Index screen was implemented.
- Bottom navigation using Navigation component was added.
- Splash screen was implemented.
- Crashlytics was added for reporting errors and log in release build variant.
- A basic retry on error implementation was added.
- Loading and error views were implemented.
- LeakCanary library was added for detecting memory leaks.
- A button was added in the app to force a widget update.
- Click on widget opens the app.
- Widget uses icons to display information instead of text.
- New app icon.
- [Gradle Versions Plugin](https://github.com/ben-manes/gradle-versions-plugin) to manage dependency updates.
- Notifications send from the widget update service:
   - To let the user know that location service is not available with an action to open location settings.
   - To let the user know that location permission is not granted with an action to open the app.

### Changed
- Kotlin Android Extensions were exchanged for Android Jetpack View Binding.
- Retrofit HttpException is not propagated outside data source layer anymore.
- Fragment shows dialogs and receives results instead instead of activity.
- Loading view was refactored to a custom view.
- Duplication in models and mappers was fixed.
- Widget ids are not stored anymore, they are requested to the system.
- [grandcentrix GmbH Android Code Style](https://github.com/grandcentrix/AndroidCodeStyle) was updated.
- Java compatibility code was updated to Java 1.8 and as [a consequence](https://stackoverflow.com/a/52585894/5189200) Kotlin JDK to 1.8, too.

### Fixed
- Listeners should be weak references in general to avoid strong reference cycles.
- R8 and release build variant are working.

## [0.1.0] - 2018-11-29
### Added
- Initial release with a simple UI and widget.
- This CHANGELOG file.
- README file.
- LICENSE file.
